package model;


import java.util.ArrayList;
import java.util.List;

import board.Square;
import board.SquareItem;
import model.piece.Piece;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ChessBoardTest {

    ChessBoard board = new ChessBoard();
    Square a8 = new Square(0, 0);
    Square e1 = new Square(7, 4);
    Square e4 = new Square(4, 4);
    Square h3 = new Square(5, 7);
    Square h1 = new Square(7, 7);
    Square g7 = new Square(1, 6);
    Square h8 = new Square(0, 7);


    /**
     * Ensures that values of the standard-setup corresponds with the expected values of a real chess-setup.
     */
    @Test
    public void standardSetUpIsGood() {
        //empty squares are null
        assertNull(board.get(e4));
        assertNull(board.get(h3));
        assertTrue(e4 instanceof Square);
        assertTrue(h3 instanceof Square);

        //type testing
        assertEquals(board.get(e1).getType(), 'K');
        assertEquals(board.get(e1).getType(), 'K');
        assertEquals(board.get(a8).getType(), 'R');
        assertEquals(board.get(h1).getType(), 'R');
        assertEquals(board.get(g7).getType(), 'p');

        //color
        assertEquals(board.get(e1).getColor(), 'w');
        assertEquals(board.get(e1).getColor(), 'w');
        assertEquals(board.get(h1).getColor(), 'w');
        assertEquals(board.get(a8).getColor(), 'b');
        assertEquals(board.get(g7).getColor(), 'b');
    }


    /**
     * Ensures that we can empty the board. I use this for testing in various classes.
     */
    @Test
    public void clearBoardTest() {
        assertEquals(board.get(a8).getType(), 'R');
        assertEquals(board.get(h1).getType(), 'R');
        assertEquals(board.get(g7).getType(), 'p');
        board.clearBoard();

        for (SquareItem<Piece> sq : board) {
            assertNull(board.get(sq.getSquare()));
        }
    }


    /**
     * Clears the board and fills it up again with standard pieces.
     */
    @Test
    public void setStandardTest() {
        board.clearBoard();

        for (SquareItem<Piece> sq : board) {
            assertNull(board.get(sq.getSquare()));
        }

        board.setStandard();
        assertEquals(board.get(a8).getType(), 'R');
        assertEquals(board.get(h1).getType(), 'R');
        assertEquals(board.get(g7).getType(), 'p');
    }


    /**
     * Ensures that the string of the board corresponds to the real board.
     */
    @Test
    public void asStringTester() {
        assertEquals(board.get(h8).getType(), 'R');
        assertNull(board.get(h3));
        assertEquals(board.get(h1).getType(), 'R');


        String stringversion = ChessBoard.asString(board);

        assertEquals(stringversion.charAt(7), 'R');
        assertEquals(stringversion.charAt(24), '*');
        assertEquals(stringversion.charAt(stringversion.length()-2), 'R');
    }

}
