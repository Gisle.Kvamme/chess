package model.ai;

import board.Square;
import model.ChessBoard;
import model.ChessModel;
import model.piece.Pawn;
import model.piece.Piece;
import model.piece.StandardPiece;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class ComputerTest {

    ChessModel model = new ChessModel(true);
    ChessBoard board = new ChessBoard();
    Computer cpu = new Computer();

    Square a4 = new Square(4, 0);
    Square b6 = new Square(2, 1);
    Square d6 = new Square(2, 3);

    Square e1 = new Square(7, 4);
    Square e2 = new Square(6, 4);
    Square e3 = new Square(5, 4);
    Square e4 = new Square(4, 4);
    Square e5 = new Square(3, 4);
    Square e6 = new Square(2, 4);
    Square e7 = new Square(1, 4);
    Square e8 = new Square(0, 4);

    Square g3 = new Square(5, 6);
    Square g4 = new Square(4, 6);
    Square h3 = new Square(5, 7);

    Square f8 = new Square(0, 5);
    Square g8 = new Square(0, 6);
    Square h8 = new Square(0, 7);
    Square f1 =new Square(7, 5);
    Square g1 = new Square(7, 6);
    Square h1 = new Square(7, 7);


    private void setup(ChessBoard board) {
        model.setCurrentBoard(board);
    }

    private void restart() {
        board = new ChessBoard();
    }


    @Test
    public void startingPositionIsEqual() {
        setup(board);

        assertEquals(cpu.evaluateScore(board), 0);
    }


    /**
     * white pushes e4, gaining control of central squares and opening up his bishop and queen's diagonals. they should gain a positional advantage purely based on this.
     */
    @Test
    public void simplePositionalAdvantage() {
        setup(board);
        assertEquals(cpu.evaluateScore(board), 0);

        Piece kingPawn = board.get(e2);
        model.move(kingPawn, e4);
        assertNotEquals(cpu.evaluateScore(board), 0);
    }


    /**
     * For reference:
     * https://i.imgur.com/kBPfJoc.png
     *
     * A more subtle position:<br>
     * Black has slightly compromised his king safety, but has some exchange in activity.<br>
     * Black's rook is more active, but white's rook is a better defender of the king.<br>
     * The knights have a similar relationship.<br><br>
     * According to the rules of my evaluation, black should be very slightly better.
     **/
    @Test
    public void moreComplexPositionalAdvantage() {
        board.clearBoard();

        //white pieces go here:
        board.set(g3, new StandardPiece('w', 'R', g3));
        board.set(g4, new StandardPiece('w', 'N', g4));
        board.set(h3, new StandardPiece('w', 'K', h3));

        //black pieces go here:
        board.set(a4, new StandardPiece('b', 'R', a4));
        board.set(d6, new StandardPiece('b', 'N', d6));
        board.set(b6, new StandardPiece('b', 'K', b6));


        setup(board);
        assertTrue(cpu.evaluateScore(board) < 0);  //actual eval: -4
    }


    /**
     * We remove one of black's pawns from the starting position. Although there is some exchange is activity; "a pawn is a pawn" as they say.
     */
    @Test
    public void simpleMaterialAdvantage() {
        board.set(e7, null);
        setup(board);
        assertTrue(cpu.evaluateScore(board) > 0);

    }


    /**
     * A safe king is better, the evaluation should check for this.<br>
     * We first have a completely equal position, with kings being exposed on the e-file.<br>
     * White then moves his king to safety, and we expect them to have a better score.
     */
    @Test
    public void kingSafetyTest() {
        board.set(e2, null);
        board.set(f1, null);
        board.set(f8, null);
        board.set(e7, null);

        setup(board);
        int initialEval = cpu.evaluateScore(board);

        model.move(board.get(e1), f1);
        setup(board);

        int newEval = cpu.evaluateScore(board);

        assertTrue(newEval > initialEval);
    }


    /**
     * Ensures that the CPU is actually capable of making some kind of move, which affects some board.
     */
    @Test
    public void canActuallyMove() {
        ChessBoard initialBoard = new ChessBoard();

        setup(board);
        Move random = cpu.getRandomMove(board, 'w');
        model.move(random.getPiece(), random.getSquare());
        setup(board);

        assertNotEquals(ChessBoard.asString(initialBoard), ChessBoard.asString(board));
    }


    //THIS FAILS BECAUSE THE AI SUCKS LOL
    @Test
    public void willCaptureHangingPiece() {
        board.clearBoard();

        //white pieces go here:
        board.set(g3, new StandardPiece('w', 'R', g3));
        board.set(g4, new StandardPiece('w', 'N', g4));
        board.set(h3, new StandardPiece('w', 'K', h3));

        //add a bunch of distraction pieces for white to make it slightly more complex
        for (int i = 0; i < 8; i++) {
            board.set(new Square(7, i), new Pawn('w', new Square(7, i)));
        }

        //black pieces go here:
        board.set(a4, new StandardPiece('b', 'R', a4));
        board.set(d6, new StandardPiece('b', 'N', d6));
        board.set(b6, new StandardPiece('b', 'K', b6));
        board.set(e5, new StandardPiece('b', 'N', e5)); //hanging piece!


        setup(board);
        System.out.println(ChessBoard.asString(board));
        Move random = cpu.getSmartMove(board, 'w');
        model.move(random.getPiece(), random.getSquare());
        setup(board);
        assertNull(board.get(e5));
    }


    /**
     * The AI can should be able to see one move ahead. I will therefore set up a bait piece, which can be easily captured by the queen, but would result in the queen getting captured immediately afterwards.
     */
    @Test
    public void seesOneMoveAhead() {
        board.clearBoard();
        board.set(e2, new StandardPiece('w', 'K', e2));
        board.set(e3, new StandardPiece('w', 'Q', e3));
        board.set(e8, new StandardPiece('b', 'K', e8));
        board.set(e7, new StandardPiece('b', 'N', e7));

        for (int i = 0; i < 8; i++) {
            board.set(new Square(0, i), new Pawn('w', new Square(0, i)));
        }
        
        setup(board);
        
        Move dontCapturePlz = cpu.getSmartMove(board, 'w');
        model.move(dontCapturePlz.getPiece(), dontCapturePlz.getSquare());

        assertEquals('N', board.get(e7).getType());
    }

}
