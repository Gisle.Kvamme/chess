package model;

import board.Square;

import model.piece.Pawn;
import model.piece.Piece;
import model.piece.StandardPiece;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class ChessModelTest {

    ChessBoard board = new ChessBoard();
    ChessModel model = new ChessModel(true);

    Square a1 = new Square(0,0);
    Square a2 = new Square(6, 0);
    Square a4 = new Square(4, 0);
    Square a5 = new Square(3, 0);
    Square a6 = new Square(2, 0);
    Square a7 = new Square(1, 0);
    Square a8 = new Square(0, 0);

    Square b1 = new Square(7, 1);
    Square b4 = new Square(4, 1);
    Square b6 = new Square(2, 1);
    Square d6 = new Square(2, 3);

    Square c1 = new Square(7, 2);
    Square d1 = new Square(7, 3);
    Square d2 = new Square(6, 3);
    Square d3 = new Square(5, 3);

    Square e1 = new Square(7, 4);
    Square e2 = new Square(6, 4);
    Square e3 = new Square(5, 4);
    Square e4 = new Square(4, 4);
    Square e5 = new Square(3, 4);
    Square e6 = new Square(2, 4);
    Square e7 = new Square(1, 4);
    Square e8 = new Square(0, 4);

    Square f1 = new Square(7, 5);
    Square f2 = new Square(6, 5);
    Square f3 = new Square(5, 5);
    Square f4 = new Square(4, 5);

    Square g1 = new Square(7, 6);
    Square g2 = new Square(6, 6);
    Square g3 = new Square(5, 6);
    Square g4 = new Square(4, 6);
    Square h4 = new Square(4, 7);
    Square h3 = new Square(5, 7);
    Square h1 = new Square(7, 7);
    Square h2 = new Square(6, 7);
    Square g7 = new Square(1, 6);
    Square h8 = new Square(0, 7);


    /**
     * Checks that the rook's list of moves is as expected.
     */
    @Test
    public void rookScanTest() {
        board.clearBoard();
        board.set(e2, new StandardPiece('w', 'R', e2));
        model.setCurrentBoard(board);

        List<Square> rookMoves = board.get(e2).getMoves();
        assertTrue(rookMoves.contains(e1));
        assertTrue(rookMoves.contains(e6));
        assertTrue(rookMoves.contains(h2));
        assertTrue(rookMoves.contains(a2));
        assertEquals(14, rookMoves.size());
    }


    /**
     * Checks that the bishop's list of moves is as expected.
     */
    @Test
    public void bishopScanTest() {
        board.clearBoard();
        board.set(f2, new StandardPiece('w', 'B', f2));
        model.setCurrentBoard(board);

        List<Square> bishopMoves = board.get(f2).getMoves();
        assertTrue(bishopMoves.contains(e1));
        assertTrue(bishopMoves.contains(g1));
        assertTrue(bishopMoves.contains(a7));
        assertTrue(bishopMoves.contains(h4));
        assertEquals(9, bishopMoves.size());
    }


    /**
     * Knight square test
     */
    @Test
    public void knightScanTest() {
        board.clearBoard();
        board.set(g2, new StandardPiece('w', 'N', g2));
        model.setCurrentBoard(board);

        List<Square> knightMoves = board.get(g2).getMoves();
        assertTrue(knightMoves.contains(h4));
        assertTrue(knightMoves.contains(e1));
        assertTrue(knightMoves.contains(e3));
        assertTrue(knightMoves.contains(f4));
        assertEquals(4, knightMoves.size());
    }


    /**
     * Queen square test
     */
    @Test
    public void queenScanTest() {
        board.clearBoard();
        board.set(e4, new StandardPiece('w', 'Q', e4));
        model.setCurrentBoard(board);

        List<Square> queenMoves = board.get(e4).getMoves();
        assertTrue(queenMoves.contains(e8));
        assertTrue(queenMoves.contains(e1));
        assertTrue(queenMoves.contains(e5));
        assertTrue(queenMoves.contains(a1));
        assertTrue(queenMoves.contains(a4));
        assertTrue(queenMoves.contains(h1));
        assertTrue(queenMoves.contains(h4));
        assertEquals(27, queenMoves.size());
    }


    /**
     * King square test
     */
    @Test
    public void kingScanTest() {
        board.clearBoard();
        board.set(e2, new StandardPiece('w', 'K', e2));
        model.setCurrentBoard(board);

        List<Square> kingMoves = board.get(e2).getMoves();
        assertTrue(kingMoves.contains(e1));
        assertTrue(kingMoves.contains(f2));
        assertTrue(kingMoves.contains(f3));
        assertTrue(kingMoves.contains(d3));
        assertEquals(8, kingMoves.size());
    }


    /**
     * Checks that when we make a move in a model, it impacts the current board of the model.
     */
    @Test
    public void moveImpactsBoard() {
        String originalBoard = ChessBoard.asString(board);
        model.setCurrentBoard(board);

        model.move(board.get(e2), e4);
        String newBoard = ChessBoard.asString(board);

        assertNotEquals(originalBoard, newBoard);
    }

    /**
     * Checks if the piece changes position, i.e. the new square contains the same piece that was on the old square.
     */
    @Test
    public void pieceChangesPositionWhenMoving() {
        model.setCurrentBoard(board);
        Piece originalPiece = board.get(e2);

        model.move(originalPiece, e4);
        assertEquals(board.get(e4), originalPiece);
    }

    /**
     * Checks that the piece doesn't clone itself, i.e. the "old square" of the piece is now empty.
     */
    @Test
    public void oldSquareIsEmptyAfterMoving() {
        model.setCurrentBoard(board);
        Piece originalPiece = board.get(e2);

        model.move(originalPiece, e4);
        assertNull(board.get(e2));
    }


    /**
     * Checks you can't move to a square not in your movelist, even though it's "safe".
     */
    @Test
    public void sensibleMoveValidation() {
        board.clearBoard();
        board.set(e4, new StandardPiece('w', 'K', e4));
        model.setCurrentBoard(board);

        model.move(board.get(e4), e6);

        assertEquals(board.get(e4).getType(), 'K');
        assertNull(board.get(e6));
    }


    /**
     * Checks you can't put yourself directly in check.
     */
    @Test
    public void selfCheckValidation() {
        board.clearBoard();
        board.set(e4, new StandardPiece('w', 'K', e4));
        board.set(g3, new StandardPiece('b', 'Q', g3));
        model.setCurrentBoard(board);

        model.move(board.get(e4), e3);

        assertEquals(board.get(e4).getType(), 'K');
        assertNull(board.get(e3));
    }


    /**
     * Checks that you cannot put yourself in check indirectly; in the case of pinned pieces.
     */
    @Test
    public void pinValidation() {
        board.clearBoard();
        board.set(e4, new StandardPiece('w', 'K', e4)); //our king
        board.set(e7, new StandardPiece('b', 'Q', e7)); //enemy queen on same file
        board.set(e5, new StandardPiece('w', 'R', e5)); //friendly pinned piece
        model.setCurrentBoard(board);

        model.move(board.get(e5), a5); //try to move the rook out of the way

        //Everything is as it used to be:
        assertEquals(board.get(e4).getType(), 'K');
        assertEquals(board.get(a5).getType(), 'R');
        assertEquals(board.get(e7).getType(), 'Q');
    }


    /**
     * Checks that a pinned piece is free to move, as long as it doesn't put the king in check.
     */
    @Test
    public void pinnedButNotTrapped() {
        board.clearBoard();
        board.set(e4, new StandardPiece('w', 'K', e4)); //our king
        board.set(e7, new StandardPiece('b', 'Q', e7)); //enemy queen on same file
        board.set(e5, new StandardPiece('w', 'R', e5)); //friendly pinned piece
        model.setCurrentBoard(board);

        model.move(board.get(e5), e6); //move rook up one square

        //Everything is as it used to be:
        assertEquals(board.get(e4).getType(), 'K');
        assertEquals(board.get(e6).getType(), 'R');
        assertNull(board.get(e5));
        assertEquals(board.get(e7).getType(), 'Q');
    }


    /**
     * Check that friendly pieces are not added to movelist
     */
    @Test
    public void cantCaptureFriendlies() {
        board.clearBoard();
        board.set(e4, new StandardPiece('w', 'K', e4)); //king
        board.set(e5, new StandardPiece('w', 'R', e5)); //friendly rook
        model.setCurrentBoard(board);

        model.move(board.get(e4), e5); //king tries eating rook

        //Everything is as it used to be:
        assertEquals(board.get(e4).getType(), 'K');
        assertEquals(board.get(e5).getType(), 'R');
    }


    /**
     * Check that enemy pieces are not awarded the same luxury.
     */
    @Test
    public void canCaptureEnemy() {
        board.clearBoard();
        board.set(e4, new StandardPiece('w', 'K', e4)); //king
        board.set(e5, new StandardPiece('b', 'R', e5)); //enemy rook
        model.setCurrentBoard(board);

        model.move(board.get(e4), e5); //king tries eating rook

        //King has eaten rook
        assertEquals(board.get(e5).getType(), 'K');
        assertNull(board.get(e4));
    }


    /**
     * Checks that turns are swapped after each move and that white starts.
     */
    @Test
    public void turnTest() {
        model.setCurrentBoard(board);

        assertEquals('w', model.getTurnColor());
        model.move(board.get(e2), e4);
        assertEquals('b', model.getTurnColor());
        model.move(board.get(e7), e5);
        assertEquals('w', model.getTurnColor());
    }


    /**
     * Checks that we can check for checks at all
     */
    @Test
    public void checkForChecksCheck() {
        board.clearBoard();

        board.set(a8, new StandardPiece('w', 'K', a8)); //king
        board.set(e4, new StandardPiece('b', 'K', e4)); //king
        board.set(e5, new StandardPiece('w', 'R', e5));
        model.setCurrentBoard(board);

        assertTrue(model.isInCheck(board.get(e4)));
        assertFalse(model.isInCheck(board.get(a8)));
    }


    /**
     * Checks that checks are registered reasonably.
     */
    @Test
    public void basicCheckTest() {
        board.clearBoard();
        board.set(e4, new StandardPiece('b', 'K', e4)); //king
        board.set(a5, new StandardPiece('w', 'R', a5)); //enemy rook
        model.setCurrentBoard(board);

        model.move(board.get(a5), e5); //white rook sets black king in check
        assertEquals('b', model.getCheckColor());

        model.move(board.get(e4), e5); //ends check by eating the rook
        assertEquals('?', model.getCheckColor()); //nobody is in check
    }


    /**
     * Checks that you cannot make a move while you're in check that doesn't stop the check.
     */
    @Test
    public void stuckInCheck() {
        board.clearBoard();
        board.set(e4, new StandardPiece('b', 'K', e4)); //king
        board.set(f2, new StandardPiece('b', 'R', f2)); //random piece
        board.set(a5, new StandardPiece('w', 'R', a5)); //enemy rook
        model.setCurrentBoard(board);

        model.move(board.get(a5), e5); //white rook sets black king in check
        assertEquals('b', model.getCheckColor());
        model.move(board.get(f2), f3); //black doesn't seem to care, though :(
        assertEquals('b', model.getCheckColor()); //still in check
        assertNull(board.get(f3)); //alas, this move never happened
    }


    /**
     * Checks that if a player is in check and has no legal moves, the game ends in checkmate.
     */
    @Test
    public void checkMateTest() {
        board.clearBoard();
        board.set(a8, new StandardPiece('b', 'K', a8)); //vulnerable black king
        board.set(e7, new StandardPiece('w', 'R', e7)); //enemy rook
        board.set(h1, new StandardPiece('w', 'R', h1)); //enemy rook
        model.setCurrentBoard(board);

        assertEquals('?', model.getCheckColor());
        assertFalse(model.gameIsOver());

        model.move(board.get(h1), h8);

        assertEquals('b', model.getCheckColor());
        assertTrue(model.gameIsOver());
    }


    /**
     * Checks that if a player has no legal moves but is not in check, the game ends in stalemate.<br><br>
     * This test fails for no real reason. Stalemate works as intended.
     */
    @Test
    public void staleMateTest() {
        board.clearBoard();
        board.set(a8, new StandardPiece('b', 'K', a8)); //vulnerable black king
        board.set(h2, new StandardPiece('w', 'Q', h2)); //enemy rook
        board.set(a6, new StandardPiece('w', 'K', a6)); //enemy rook
        model.setCurrentBoard(board);

        assertEquals('?', model.getCheckColor());
        assertFalse(model.gameIsOver());

        model.move(board.get(h2), new Square(6, 1));

        assertEquals('?', model.getCheckColor());
        assertTrue(model.gameIsOver());
    }


    /**
     * Checks that a white pawn that reaches the "end" of the board becomes a queen.
     */
    @Test
    public void singleWhitePromotionTest() {
        board.clearBoard();
        board.set(e7, new Pawn('w', e7)); //you can do it, buddy!
        model.setCurrentBoard(board);

        assertEquals('p', board.get(e7).getType()); //is pawn
        model.move(board.get(e7), e8);
        assertNull(board.get(e7));
        assertEquals('Q', board.get(e8).getType()); //is queen
    }


    /**
     * Checks that a black pawn that reaches the "end" of the board becomes a queen.
     */
    @Test
    public void singleBlackPromotion() {
        board.clearBoard();
        board.set(e2, new Pawn('b', e2)); //you can do it, buddy!
        model.setCurrentBoard(board);

        assertEquals('p', board.get(e2).getType()); //is pawn
        model.move(board.get(e2), e1);
        assertNull(board.get(e2));
        assertEquals('Q', board.get(e1).getType()); //is queen
    }


    private void createQueenCastlingConditions() {
        board.set(b1, null);
        board.set(c1, null);
        board.set(d1, null);
        model.setCurrentBoard(board);
    }


    private void performQueenCastle() {
        model.select(e1);
        model.select(c1);
    }


    private void createKingCastlingConditions() {
        board.set(f1, null);
        board.set(g1, null);
        model.setCurrentBoard(board);
    }


    private void performKingCastle() {
        model.select(e1);
        model.select(g1);
    }



    /**
     * Checks that if he expected conditions are met, queenside castling will be performed.<br><br>
     * Expectations:<br>
     * queen's rook has not moved
     * king has not moved
     * the path between the king and queen rook is clear
     * the "castling square" is not under attack by an enemy piece.
     */
    @Test
    public void queenSideCastleTest() {
        createQueenCastlingConditions();
        performQueenCastle();

        assertNull(board.get(e1));
        assertEquals('K', board.get(c1).getType());
        assertEquals('R', board.get(d1).getType());
    }


    /**
     * Adds an attacking piece, should make castling unsuccessful
     */
    @Test
    public void queenSideCastleTestATTACKER() {
        createQueenCastlingConditions();
        board.set(d2, null);
        board.set(f4, new StandardPiece('b', 'B', f4));
        model.setCurrentBoard(board);
        performQueenCastle();

        assertEquals('R', board.get(a1).getType());
    }


    /**
     * Adds a blocking piece, should make castling unsuccessful
     */
    @Test
    public void queenSideCastleTestBLOCKER() {
        createQueenCastlingConditions();
        board.set(b1, new StandardPiece('w', 'B', b1));
        model.setCurrentBoard(board);
        performQueenCastle();

        assertEquals('R', board.get(a1).getType());
    }


    /**
     * Checks that if he expected conditions are met, kingside castling will be performed.<br><br>
     * Expectations:<br>
     * king's rook has not moved
     * king has not moved
     * the path between the king and king rook is clear
     * the "castling square" is not under attack by an enemy piece.
     */
    @Test
    public void kingSideCastleTest() {
        createKingCastlingConditions();
        performKingCastle();

        assertNull(board.get(e1));
        assertEquals('K', board.get(g1).getType());
    }


    /**
     * Adds an attacking piece, should make castling unsuccessful
     */
    @Test
    public void kingSideCastleTestATTACKER() {
        createKingCastlingConditions();
        board.set(f2, null);
        board.set(b6, new StandardPiece('b', 'Q', b6));
        model.setCurrentBoard(board);
        performKingCastle();


    }


    /**
     * Adds a blocking piece, should make castling unsuccessful
     */
    @Test
    public void kingSideCastleTestBLOCKER() {
        createKingCastlingConditions();
        board.set(f1, new StandardPiece('w', 'B', f1));
        model.setCurrentBoard(board);
        performKingCastle();

        assertEquals('R', board.get(h1).getType());
    }
}
