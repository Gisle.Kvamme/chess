package board;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BoardTest {

    /**
     * The constructor creates a board of expected dimensions
     */
    @Test
    void boardConstructorTest() {
        IBoard<Object> board = new Board<>(4, 20);
        assertEquals(4, board.getRanks());
        assertEquals(20, board.getFiles());
    }

    /**
     * The secondary constructor functions, and the board holds expected and consistent values.
     */
    @Test
    void boardFillTest() {
        //we can fill a board with anything
        IBoard<Integer> board = new Board<>(20, 20, 9001);

        assertEquals(9001, board.get(new Square(3, 2)));
        assertEquals(9001, board.get(new Square(1, 1)));

        //but the values are still not "locked"
        board.set(new Square(1, 1), null);

        assertNull(board.get(new Square(1, 1)));
        assertEquals(9001, board.get(new Square(5, 6)));
        assertEquals(9001, board.get(new Square(4, 7)));
        assertEquals(9001, board.get(new Square(3, 8)));
    }

    /**
     * The default constructor actually fills the board with null-values.
     */
    @Test
    void defaultConstructorYieldsNullTest() {
        IBoard<Object> board = new Board<>(9, 9);

        assertNull(board.get(new Square(0, 0)));
        assertNull(board.get(new Square(2, 1)));

        board.set(new Square(1, 1), "I hate writing Junit tests");

        assertEquals("I hate writing Junit tests", board.get(new Square(1, 1)));
        assertNull(board.get(new Square(0, 1)));
        assertNull(board.get(new Square(1, 0)));
        assertNull(board.get(new Square(2, 1)));
    }

    @Test
    void coordinateIsOnGridTest() {
        IBoard<Double> board = new Board<>(3, 2, 40.000);

        assertTrue(board.squareIsOnGrid(new Square(2, 1)));
        assertFalse(board.squareIsOnGrid(new Square(3, 1)));
        assertFalse(board.squareIsOnGrid(new Square(2, 2)));

        assertFalse(board.squareIsOnGrid(new Square(0, 696969)));
        assertFalse(board.squareIsOnGrid(new Square(-100, 0)));
        assertFalse(board.squareIsOnGrid(new Square(0, -10000)));
        assertFalse(board.squareIsOnGrid(new Square(3, 3)));
    }

}
