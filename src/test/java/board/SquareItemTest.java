package board;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class SquareItemTest {

    @Test
    public void constructorTest() {
        SquareItem<Integer> square1 = new SquareItem<>(new Square(4, 20), 9001);
        SquareItem<Integer> square2 = new SquareItem<>(new Square(2, 22), 9001);

        assertNotNull(square1.getPiece());
        assertNotNull(square2.getPiece());
        assertNotNull(square1.getSquare());
        assertNotNull(square2.getSquare());

        assertEquals(square1.getSquare(), new Square(4, 20));
        assertEquals(square1.getSquare().getRank(), 4);
        assertEquals(square2.getSquare().getFile(), 22);
        assertEquals(square2.getPiece(), 9001);


    }

    //taken from my personal tetris assignment
    @Test
    void testIterator() {
        IBoard<String> board = new Board<>(3, 2, "x");
        board.set(new Square(0, 0), "a");
        board.set(new Square(1, 1), "b");
        board.set(new Square(2, 1), "c");

        List<SquareItem<String>> items = new ArrayList<>();
        for (SquareItem<String> SquareItem : board) {
            items.add(SquareItem);
        }

        assertEquals(3 * 2, items.size());
        assertTrue(items.contains(new SquareItem<String>(new Square(0, 0), "a")));
        assertTrue(items.contains(new SquareItem<String>(new Square(1, 1), "b")));
        assertTrue(items.contains(new SquareItem<String>(new Square(2, 1), "c")));
        assertTrue(items.contains(new SquareItem<String>(new Square(0, 1), "x")));
    }
}
