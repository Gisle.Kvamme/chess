package model;

import view.ChessViewable;
import controller.ChessControllable;
import board.Square;
import board.SquareItem;
import model.ai.Computer;
import model.ai.Move;
import model.piece.*;

import java.util.ArrayList;
import java.util.List;


/**
 * The interactable chess model, on which all gameplay occurs.<br><br>
 * This should really have been split into several classes, but I realised it a bit too late, and now it would take a lot of time (maybe).<br>
 * Sorry.
 */
public class ChessModel implements ChessViewable, ChessControllable {


    private static ChessBoard board = new ChessBoard();
    private static Piece whiteKing;
    private static Piece blackKing;
    private Piece selectedPiece;
    private Turn toPlay;
    private Check inCheck = Check.NONE;
    private boolean vsComputer;
    private final Computer AIPlayer = new Computer();


    /**
     * If testing, or doing computer calculations, we only deal with the "gameplay" aspect, i.e. no menus.
     */
    public ChessModel(boolean testing)  {
        startGame();
        setAllMovement();
        if (!testing)
            toPlay = Turn.MENU;
    }


    /**
     * Get whose turn it is.
     */
    public char getTurnColor() {
        return (toPlay.equals(Turn.WHITE)) ? 'w' : 'b';
    }


    /**
     * Gets the king whose color corresponds to the turn-color.
     */
    private Piece getOurKing() {
        return (toPlay.equals(Turn.WHITE)) ? whiteKing : blackKing;
    }


    /**
     * Switches whose turn it is to play, clears checks and selected pieces.<br><br>
     * Since we swap a the end of turns, this is where it makes sense to check for stalemates and checkmaters.<br><br>
     * Also if we are playing against an AI, we generate a move for them immediately.
     */
    private void swapTurn() {
        //SWAPPING TURN
        toPlay = (toPlay.equals(Turn.WHITE)) ? Turn.BLACK : Turn.WHITE;

        //CLEARING GARBAGE FROM PAST ROUNDS
        selectedPiece = null;
        setKingInCheck('?');

        //CHECKMATE AND STALEMATE TESTS
        if (isInCheck(getOurKing())) {
            if (checkmateTest())
                checkMate();
            else
                setKingInCheck(getOurKing().getColor());
        } else if (staleMateTest()) {
            staleMate();
        }

        //COMPUTER PLAYER
        if (toPlay.equals(Turn.BLACK) && vsComputer) {
            toPlay = Turn.WAITING;
            makeComputerMove('b');
            toPlay = Turn.WHITE;
        }
    }


    /**
     * Simplified swap that only changes the enum's state.
     */
    protected void forceTurnSwap() {
        toPlay = (toPlay.equals(Turn.WHITE)) ? Turn.BLACK : Turn.WHITE;
    }


    /**
     * Change the current board in the model to a different one.<br>
     * This is usable for testing, and for the computer player, which creates
     * a copy of the board to manipulate.
     */
    public void setCurrentBoard(ChessBoard newBoard) {board = newBoard;
    setAllMovement();}


    /**
     * Stops gameplay and sets the game in a state where no player can move.
     */
    private void stopTurns() {
        toPlay = Turn.GAMEOVER;
    }


    /**
     * If checkmate has been reached, we end the game. We set in the king in check "again" to better visually demonstrate that the player has lost.
     */
    private void checkMate() {
        setKingInCheck(getOurKing().getColor());
        stopTurns();
    }


    /**
     * If stalemate has been reached, we end the game.
     */
    private void staleMate() {
        setKingInCheck('?');
        stopTurns();
    }


    /**
     * See the entire {@link Computer} class. <br><br>
     * The AI sucks so much that I generate a few different results to see what works best.
     */
    private void makeComputerMove(char color) {
        /**
        Move[] moves = new Move[]{AIPlayer.getSmartMove(board, color), AIPlayer.getSmartMove(board, color), AIPlayer.getSmartMove(board, color)};


        int smartestMoveEval = (color == 'w') ? -1000 : 1000;
        Move smartestMove = moves[0];
        for (int i = 1; i < 3; i++) {
            if ((AIPlayer.evaluateAfterMove(board, moves[i])) < smartestMoveEval) {
                smartestMoveEval = AIPlayer.evaluateAfterMove(board, moves[i]);
                smartestMove = moves[i];
            }
        }

        move(smartestMove.getPiece(), smartestMove.getSquare());

         */
        Move mv = AIPlayer.getSmartMove(board, 'b');
        move(mv.getPiece(), mv.getSquare());
    }


    /**
     * Standard move-method. Should be called after {@link ChessModel#validateMove(Piece, Square)}.
     * First updates the state of the board to make sure it's correct.<br>
     * Then changes the board directly to empty the square where the piece was standing, and change its position to a new square.<br>
     * We update the piece's "square" field variable so we can know where the piece is located.<br>
     * Then we swap turns, so that the other player may play and update the board state again.
     * <br><br>
     * @param p - the moving piece
     * @param toSquare - the square to move to
     */
    public void move(Piece p, Square toSquare) {
        setAllMovement();
        if (p!= null  &&  p.getMoves().contains(toSquare)  &&  validateMove(p, toSquare)) {
            board.set(p.getSquare(), null);
            board.set(toSquare, p);

            p.pieceHasMoved();
            p.setSquare(toSquare);
            promotionTest();
            setAllMovement();
            swapTurn();
        }
        setAllMovement();
    }


    /**
     * Forces a move, which means that we don't check if the move is "legal" or part of the piece's move-list.<br>
     * This is a dangerous method! I use it only for castling, (example: {@link ChessModel#queenSideCastle()}, but also for testing.
     */
    protected void forceMove(Piece p, Square toSquare) {
        board.set(p.getSquare(), null);
        board.set(toSquare, p);
        p.pieceHasMoved();
        p.setSquare(toSquare);
        setAllMovement();
    }


    /**
     * Loops through the board and checks if any pawns have reached the top, performed after every move.<br>
     * Wanted to have this be done in a similar way to castling, where a piece moves, and we check if it's a pawn and so on, but it refused to work.
     */
    private void promotionTest() {
        for (SquareItem<Piece> sq : board) {
            if (sq.getPiece() instanceof Pawn) {
                if (sq.getSquare().getRank() == 0 && sq.getPiece().getColor() == 'w') {
                    board.set(sq.getSquare(), new StandardPiece('w', 'Q', sq.getSquare()));
                } else if (sq.getSquare().getRank() == 7 && sq.getPiece().getColor() == 'b') {
                    board.set(sq.getSquare(), new StandardPiece('b', 'Q', sq.getSquare()));
                }
            }
        }
    }


    /**
     * Checks if a player can get out of check. This is done by using {@link ChessModel#validateMove(Piece, Square)} on any possible move.<br>
     * Because if a move is validated, that means that we can perform a move which does not result in a check towards our king.<br><br>
     * This is not a very efficient method, as it performs tons of unnecessary for-loops, but you're not allowed to deduct points for that in this course, hehe. :--)
     */
    private boolean checkmateTest() {
        char ourColor = getOurKing().getColor();

        for (SquareItem<Piece> square : board) {

                Piece p = square.getPiece();
                if (pieceIsFriendlyTo(p, ourColor)) {
                   for (Square sq : p.getMoves()) {
                       if (validateMove(p, sq))
                           return false;
                   }
                }
            }
            return true;
        }


    /**
     * @param color - the king of this color will be placed in check
     */
    private void setKingInCheck(char color) {
        if (color == 'w')
            inCheck = Check.WHITE;
        else if (color == 'b')
            inCheck = Check.BLACK;
        else
            inCheck = Check.NONE;
    }


    /**
     * Performed after every turn to check if "stalemate" has been reached. That is, it's one player's turn to move, they don't have any legal moves, and they are not in check. This results in a draw.
     */
    private boolean staleMateTest() {
        char ourColor = getOurKing().getColor();

        for (SquareItem<Piece> square : board) {
            Piece p = square.getPiece();

            if (pieceIsFriendlyTo(p, ourColor)) {
                if (pieceHasLegalMoves(p))
                    return false;
            }
        }
        return true;
    }


    /**
     * Given a specific piece, it checks if that piece has any legal moves.
     * <br>
     * Helper for {@link ChessModel#staleMateTest()}
     * <br><br>
     * @param p - the piece
     * @return - bool of whether it has legal moves
     */
    private boolean pieceHasLegalMoves(Piece p) {
        for (Square move : p.getMoves()) {
            if (validateMove(p, move))
                return true;
        }
        return false;
    }


    /**
     * Takes a piece and checks if it matches the inputted color.
     * <br>
     * @param p the piece that may or may not be friendly
     * @param relativeColor the color that the piece may or may not be friendly to
     * @return whether it is so
     */
    private boolean pieceIsFriendlyTo(Piece p, char relativeColor) {
        return (p != null && p.getColor() == relativeColor);
    }


    /**
     * If we click on a piece:<br>
     * If we have nothing selected, and the piece matches the turncolor, we select it.<br>
     * If the piece is friendly to the already-selected piece, we switch "control" to that piece.<br>
     * If we click on the same piece that we have selected, we unselect it.<br>
     * If we click on an enemy, we must be trying to capture it.<br>
     * Otherwise nothing happens :)
     */
    private void selectedTaken(Square square, Piece piece) {
        if (selectedPiece != null  &&  square.equals(selectedPiece.getSquare()))
            selectedPiece = null;
        else if (getTurnColor() == piece.getColor())
            selectedPiece = piece;
        else
            move(selectedPiece, square);
    }


    /**
     * If a piece is trying to move to an empty square, we first investigate if this is a king attempting to castle (which is an exception case).<br>
     * If not, we just assume it's a normal move, and we attempt to complete it.
     */
    private void selectedEmpty(Square square) {
        if (selectedPiece != null && selectedPiece.getType() == 'K' && pieceIsFriendlyTo(selectedPiece, getTurnColor()) && !selectedPiece.hasMoved()) {
            castleSquareTest(square);
        } else if (pieceIsFriendlyTo(selectedPiece, getTurnColor())) {
            move(selectedPiece, square);
        }
    }


    /**
     * Confirms that the empty square that the unmoved king is trying to reach corresponds with a castling square of their corresponding color.
     * <br>
     * See complete logic in {@link ChessModel#kingSideCastle()} or {@link ChessModel#queenSideCastle()}}
     */
    private void castleSquareTest(Square square) {
        boolean onWhiteCastlingSquares = square.equals(new Square(7, 2)) || square.equals(new Square(7, 6));
        boolean onBlackCastlingSquares = square.equals(new Square(0, 2)) || square.equals(new Square(0, 6));

        if (selectedPiece.getColor() == 'w' && onWhiteCastlingSquares)
            castleValidateMoveTest(square);
        else if (selectedPiece.getColor() == 'b' && onBlackCastlingSquares)
            castleValidateMoveTest(square);
        else
            move(selectedPiece, square);
    }


    /**
     * Checks that castling is a legal move, i.e. those squares are not controlled by enemy pieces.
     * <br>
     * See complete logic in {@link ChessModel#kingSideCastle()} or {@link ChessModel#queenSideCastle()}}
     */
    private void castleValidateMoveTest(Square targetSquare) {
        int rank = (selectedPiece.getColor() == 'w') ? 7 : 0;

        if (targetSquare.equals(new Square(rank, 2)) && validateMove(selectedPiece, targetSquare)) {
            queenSideSpaceTest();
        } else if (targetSquare.equals(new Square(rank, 6)) && validateMove(selectedPiece, targetSquare)) {
            kingSideSpaceTest();
        } else {
            selectedPiece = null;
        }
    }


    /**
     * Checks that the king that wants to castle queen-side can logically do so, i.e. there's nothing blocking it and the rook there has not moved.
     * <br>
     * See complete logic in {@link ChessModel#kingSideCastle()} or {@link ChessModel#queenSideCastle()}}
     */
    private void queenSideSpaceTest() {
        int rank = (selectedPiece.getColor() == 'w') ? 7 : 0;
        Square qRookSquare = new Square(rank, 0);

        boolean rookHasNotMoved = !isEmpty(qRookSquare) && !board.get(qRookSquare).hasMoved();
        boolean clearPath = isEmpty(rank,1)  &&  isEmpty(rank, 2)  &&  isEmpty(rank, 3);

        if (rookHasNotMoved && clearPath) {
            queenSideCastle();
        }
    }


    /**
     * Checks that the king that wants to castle king-side can logically do so, i.e. there's nothing blocking it and the rook there has not moved.
     * <br>
     * See complete logic in {@link ChessModel#kingSideCastle()} or {@link ChessModel#queenSideCastle()}}
     */
    private void kingSideSpaceTest() {
        int rank = (selectedPiece.getColor() == 'w') ? 7 : 0;
        Square kRookSquare = new Square(rank, 7);

        boolean rookHasNotMoved = !isEmpty(kRookSquare) && !board.get(kRookSquare).hasMoved();
        boolean clearPath = isEmpty(rank,5) && isEmpty(rank, 6);

        if (rookHasNotMoved && clearPath)
            kingSideCastle();
    }


    /**
     * There are a series of tests which must be conducted in order to ensure a successful queen-side castle:<br>
     * {@link ChessModel#selectedEmpty(Square)} - checks that you are a king that has not previously moved and want to move to an empty square
     * {@link ChessModel#castleSquareTest(Square)} - checks if the square that you are trying to reach corresponds with a castling square of your color
     * {@link ChessModel#castleValidateMoveTest(Square)} - checks if king-side castling is a legal move, i.e. those squares are not controlled by the enemy
     * {@link ChessModel#queenSideSpaceTest()} - checks if the castling squares on the king-side are free, and that the king-side's rook has not moved.
     * <br>
     * If all tests, pass, we force a king-side castle.
     */
    private void kingSideCastle() {
        int rank = (selectedPiece.getColor() == 'w') ? 7 : 0;
        forceMove(selectedPiece, new Square(rank, 6));
        forceMove(board.get(new Square(rank, 7)), new Square(rank, 5));
        swapTurn();
    }


    /**
     * There are a series of tests which must be conducted in order to ensure a successful queen-side castle:<br>
     * {@link ChessModel#selectedEmpty(Square)} - checks that you are a king that has not previously moved and want to move to an empty square
     * {@link ChessModel#castleSquareTest(Square)} - checks if the square that you are trying to reach corresponds with a castling square of your color
     * {@link ChessModel#castleValidateMoveTest(Square)} - checks if queen-side castling is a legal move, i.e. those squares are not controlled by the enemy
     * {@link ChessModel#queenSideSpaceTest()} - checks if the castling squares on the queen-side are free, and that the queenside's rook has not moved.
     * <br>
     * If all tests, pass, we force a queen-side castle.
     */
    private void queenSideCastle() {
        int rank = (selectedPiece.getColor() == 'w') ? 7 : 0;
        forceMove(selectedPiece, new Square(rank, 2));
        forceMove(board.get(new Square(rank, 0)), new Square(rank, 3));
        swapTurn();
    }


    /**
     * Static helper for castling methods, see {@link ChessModel#queenSideCastle()} & {@link ChessModel#kingSideCastle()}.
     */
    private static boolean isEmpty(Square square) {
        return (board.get(square) == null);
    }


    /**
     * Static helper for castling methods, see {@link ChessModel#queenSideCastle()} & {@link ChessModel#kingSideCastle()}.
     */
    private static boolean isEmpty(int rank, int file) {
        return (board.get(new Square(rank, file)) == null);
    }


    /**
     * Loops through the board, "updating the movement of any pieces that we come across. This is called each time anything on the board moves so that the other pieces may adjust their squares to this change in the position.
     */
    private void setAllMovement() {
        for (SquareItem<Piece> square : board) {
            Piece p = square.getPiece();

            if (p != null && p.getType() != 'p') {
                p.setMoves(movementScan(p));
            } else if (p != null && p.getType() == 'p') {
                setPawnMovement(p);
            }
        }
    }


    /**
     * Pawns have peculiar movement, so they require their own method. See all docs in {@link MovementPattern}.
     */
    private void setPawnMovement(Piece pawn) {
        MovementPattern.setColor(pawn.getColor());
        pawn.setMoves(movementScan(pawn));
        pawn.changeMovePattern();
        pawn.addMoves(movementScan(pawn));
    }


    /**
     * Performs a "scan" by taking into a 2d-array from the given piece's {@link MovementPattern} and assessing the resulting squares.<br><br>
     * If we hit a friendly piece, we stop and exclude, if we hit an enemy piece we stop and include (which makes enemies capturable).<br>
     * <br><br>
     * @param ourPiece - the piece that we are assessing the moves of
     * @return a list of squares that the piece could move to
     */
    private static List<Square> movementScan(Piece ourPiece)  {
        int len = ourPiece.getMovement().getLen();
        int[] rankMovement = ourPiece.getMovement().getRankMovement();
        int[] fileMovement = ourPiece.getMovement().getFileMovement();

        List<Square> movement = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            int newRank = ourPiece.getSquare().getRank();
            int newFile = ourPiece.getSquare().getFile();

            while (board.squareIsOnGrid(new Square(newRank+rankMovement[i], newFile+fileMovement[i]))) {
                newRank += rankMovement[i];
                newFile += fileMovement[i];

                if (!isEmpty(newRank, newFile)) {
                    if (pieceIsEnemy(ourPiece, new Square(newRank, newFile)))
                        movement.add(new Square(newRank, newFile));
                    break;

                } else {
                    if (ourPiece.getType() != 'p' || ourPiece.isMovingPawn()) {
                        movement.add(new Square(newRank, newFile));
                    }
                }
                if (!ourPiece.getMovement().isRepeated())                                             //for kings, knights, and pawns
                    break;
            }
        }
        return movement;
    }


    /**
     * A static variant of {@link ChessModel#pieceIsFriendlyTo(Piece, char)} that is specifically meant to help {@link ChessModel#movementScan(Piece)}.
     * <br>
     * @param ourPiece - the piece that is being "scanned" for moves.
     * @param square - the square of the piece that we are investigating.
     * @return whether not the piece is an enemy
     */
    public static boolean pieceIsEnemy (Piece ourPiece, Square square) {
        return board.get(square).getColor()==ourPiece.getOppositeColor() && !ourPiece.isMovingPawn();
    }



    /**
    * Returns true if the suggested move does not put your own king in check
    * Otherwise the move is illegal, and it returns false.
    * This method is resource-intensive, and will only be called when necessary.
    * Uses {link to isInCheck}
    */
    public boolean validateMove(Piece p, Square toSquare) {
        Piece ourKing = (p.getColor() == 'w') ? whiteKing : blackKing;

        Square orgSquare = p.getSquare();
        Piece orgContent = board.get(toSquare);
        board.set(toSquare, p);
        board.set(orgSquare, null);
        p.setSquare(toSquare);
        setAllMovement();

        boolean validation = !isInCheck(ourKing);

        board.set(orgSquare, p);
        p.setSquare(orgSquare);
        board.set(toSquare, orgContent);
        setAllMovement();

        return validation;
    }


    /**
     * Checks through the moves of all enemy pieces, seeing if they are "attacking" the square that "our" king is standing on. This is called a "check" in normal language.
     * <br>
     * @param k - the king that is being assessed
     * @return whether any enemy piece can "capture" the king
     */
    protected boolean isInCheck(Piece k)  {
        char oppositeColor = k.getOppositeColor();

        for (SquareItem<Piece> square : board) {
            Piece p = square.getPiece();

            if (p != null && p.getColor() == oppositeColor) {
                if (p.getMoves().contains(k.getSquare())) {
                    return true;
                }
            }
        }
        return false;
    }



    //
    //
    /*
      Implemented methods from various interfaces which faciliate the model's communication with the view or controller.
     */
    //
    //



    /**
     * Called by {@link controller.ChessController} when the "play vs. human" button is clicked in the main menu,<br>
     * which starts a normal human-human game of chess.
     */
    @Override
    public void startGame() {
        vsComputer = false;
        setKingInCheck('?');
        board.setStandard();
        whiteKing = board.get(new Square(7, 4));
        blackKing = board.get(new Square(0, 4));
        toPlay = Turn.WHITE;
        setAllMovement();
    }


    /**
     * Called by {@link controller.ChessController} when the "play vs. computer" button is clicked in the main menu.
     */
    @Override
    public void startCPUGame() {
        startGame();
        vsComputer = true;
    }


    /**
     * Called by {@link controller.ChessController} when the "return to menu" button is clicked from the game over screen.
     */
    @Override
    public void setToMainMenu() {
        toPlay = Turn.MENU;
    }


    /**
     * In {@link controller.ChessController}: Creates JLabels which function as the main menu buttons.<br>
     * In {@link view.ChessView}: Creates corresponding graphics, both the menu itself and the "visual" buttons.
     */
    @Override
    public boolean inMainMenu() {
        return (toPlay.equals(Turn.MENU));
    }


    /**
     * In {@link controller.ChessController}: Creates JLabels which function as navigators to either exiting the game or returning to the menu.<br>
     * In {@link view.ChessView}: Creates corresponding graphics, a game over screen and some buttons.
     */
    @Override
    public boolean gameIsOver() {
        return (toPlay.equals(Turn.GAMEOVER));
    }


    /**
     * In {@link controller.ChessController}: Creates JLabels which make each square clickable, enabling gameplay.<br>
     * In {@link view.ChessView}: Draws the board during gameplay with pieces included.
     */
    @Override
    public boolean gameIsActive() {
        return (toPlay.equals(Turn.WHITE) || (toPlay.equals(Turn.BLACK)));
    }


    /**
     * In {@link view.ChessView}: Draws a "Waiting for computer..." screen over the board while the computer is selecting a move.<br><br>
     * Okay, I lied, it actually doesn't -- but that was the idea :)
     */
    @Override
    public boolean playerIsWaiting() {
        return (toPlay.equals(Turn.WAITING));
    }


    /**
     * Returns the color char of the king who is in check. If no king is in check it returns a nonsense character which has no effect in the view.
     * <br>
     * In {@link view.ChessView}: Draws the king who is in check with a dramatic red glow around it, otherwise it doesn't hehe.
    */
    @Override
    public char getCheckColor() {
        if (inCheck.equals(Check.WHITE))
            return 'w';
        else if (inCheck.equals(Check.BLACK))
            return 'b';
        else
            return '?';
    }


    /**
     * If gameplay is active, and you click on one of the JLabels which represent squares, it should have "some effect"<br><br>
     * See {@link ChessModel#selectedEmpty} & {@link ChessModel#selectedTaken} for specifics.
     */
    @Override
    public void select(Square square) {
        Piece content = board.get(square);

        if (!gameIsOver()) {
            if (content == null)
                selectedEmpty(square);
            else
                selectedTaken(square, content);
        }
    }


    /**
     * In {@link view.ChessView}: Draws the selected piece with a yellow glow behind it, making it easy to see what is selected.
     */
    @Override
    public Piece getSelectedPiece() {
        return this.selectedPiece;
    }


    /**
     * In {@link controller.ChessController}: Allows have the board filled with an appropraite amount of clickable JLabels
     * In {@link view.ChessView}: Creates the board with appropriate dimensions of the board.
     */
    @Override
    public int getRanks() {
        return board.getRanks();
    }


    /**
     * In {@link controller.ChessController}: Allows have the board filled with an appropraite amount of clickable JLabels
     * In {@link view.ChessView}: Creates the board with appropriate dimensions of the board.
     */
    @Override
    public int getFiles() {
        return board.getFiles();
    }


    /**
     * In {@link controller.ChessController}: Allows us to loop through the board to identify which JLabels correspond to which in-game squares.
     * In {@link view.ChessView}: Allows us to draw the correct contents of the board in correspondence to the in-game piece's and their positions.
     */
    @Override
    public Iterable<SquareItem<Piece>> boardIterator() {
        return board;
    }
}
