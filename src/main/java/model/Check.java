package model;

/**
 * Keeps track of which player, if any, are in check.<br><br>
 * If the game is over, the player is check has lost.<br>
 * If the game is over but no player is in check, it's a draw by stalemate.
 */
public enum Check {
    WHITE,
    BLACK,
    NONE
}
