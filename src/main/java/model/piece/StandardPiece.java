package model.piece;

import board.Square;


/**
 * A "standard piece" is a made-up term that describes a piece which has no special rules tied to its movement,
 * Pawns are not standard as they don't have a constant movement-attack pattern, as their attacking squares and movement squares are mutually exclusive.<br>
 */
 public class StandardPiece extends Piece {


    /**
     * @param color - the color of the piece
     * @param type - the specified type, a different gives a different {@link MovementPattern} as a field
     * @param square - to keep track of its location throughout gameplay
     */
    public StandardPiece(char color, char type, Square square) {
        super(color, type, square);

        if (type == 'B') {
            movement = MovementPattern.Bishop;
        } else if (type == 'N') {
            movement = MovementPattern.Knight;
        } else if (type == 'Q') {
            movement = MovementPattern.Queen;
        } else if (type == 'R') {
            movement = MovementPattern.Rook;
        } else if (type == 'K') {
            movement = MovementPattern.King;
        }
    }
}
