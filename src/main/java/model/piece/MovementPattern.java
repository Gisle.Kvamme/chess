package model.piece;

/**
 * Holds a given two-dimensional movement pattern, which will then be assigned to a piece as a field variable.<br>
 * The pattern is created so that a single possible move is the common index of two arrays, rankMovement and fileMovement.<br>
 * For example: -1, +1 denotes moving up and to the right one square at a time.<br>
 * The boolean "repeated" keeps track of whether such a movement is contunious, such as with a bishop, or only once, as with the king.
 */
public class MovementPattern {

    private int[] rankMovement;
    private final int[] fileMovement;
    private final boolean repeated;


    private MovementPattern(int[] rankMovement, int[] fileMovement, boolean repeated) {
        this.rankMovement = rankMovement;
        this.fileMovement = fileMovement;
        this.repeated = repeated;
    }


    public int getLen() {return this.rankMovement.length;}
    public int[] getRankMovement() {return this.rankMovement;}
    public int[] getFileMovement() {return this.fileMovement;}
    public boolean isRepeated() {return this.repeated;}


    /**
     * Pawns are not omnidirectional, and thus their pattern must be switched at each turn-change. Calling white() makes pawns move up the board.
     */
    private static void white() {
        MovedPawn.rankMovement = new int[]{-1};
        UnmovedPawn.rankMovement = new int[]{-2, -1};
        AttackingPawn.rankMovement = new int[]{-1, -1};
    }

    /**
     * Pawns are not omnidirectional, and thus their pattern must be switched at each turn-change. Calling black() maks pawns down the board.
     */
    private static void black() {
        MovedPawn.rankMovement = new int[]{+1};
        UnmovedPawn.rankMovement = new int[]{+2, +1};
        AttackingPawn.rankMovement = new int[]{+1, +1};
    }

    /**
     * Changes the color scheme and thus calls one of the above methods.
     */
    public static void setColor(char color) {
        if (color == 'w')
            white();
        else
            black();
    }


    /**
     * Standard movement patterns for normal-rules chess.
     */
    public static MovementPattern Knight = new MovementPattern(
            new int[]{-2, -2, 2, 2, -1, -1, 1, 1},
            new int[]{-1, 1, -1, 1, -2, 2, -2, 2},
            false
    );

    public static MovementPattern Bishop = new MovementPattern(
            new int[]{-1, -1, 1, 1},
            new int[]{-1, 1, -1, 1},
            true
    );

    public static MovementPattern Rook = new MovementPattern(
            new int[]{-1, 0, 1, 0},
            new int[]{0, -1, 0, 1},
            true
    );

    public static MovementPattern Queen = new MovementPattern(
            new int[]{-1, -1, -1, 0, 0, 1, 1, 1},
            new int[]{-1, 0, 1, -1, 1, -1, 0, 1},
            true
    );

    public static MovementPattern King = new MovementPattern(
            new int[]{-1, -1, -1, 0, 0, 1, 1, 1},
            new int[]{-1, 0, 1, -1, 1, -1, 0, 1},
            false
    );


    /**
     * Pawns are a bit tricky to program! :/  The easiest solution I found was to have three of these and then just switching directions with calls to {@link MovementPattern#white()} or {@link MovementPattern#black()}
     */
    public static MovementPattern UnmovedPawn = new MovementPattern(
            new int[]{-2, -1},
            new int[]{0, 0},
            false
    );


    public static MovementPattern MovedPawn = new MovementPattern(
            new int[]{-1},
            new int[]{0},
            false
    );


    public static MovementPattern AttackingPawn = new MovementPattern(
            new int[]{-1, -1},
            new int[]{-1, +1},
            false
    );


}
