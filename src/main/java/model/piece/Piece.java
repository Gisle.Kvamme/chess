package model.piece;

import board.Square;

import java.util.List;


/**
 * Superclass for all chess Pieces, which holds various protected fields and common methods.<br><br>
 *
 * It is abstract as we cannot have a Piece that doesn't have a particular type
 */
abstract public class Piece {
    protected MovementPattern movement;
    protected boolean hasMoved;

    protected List<Square> legalMoves;
    protected char color;
    protected char oppositeColor;
    protected char type;
    protected Square square;


    /**
     * @param color - color of the piece, given as a single character, 'w' or 'b'.
     * @param type - 'N' for knights, 'B' for bishops, 'Q' for queen, 'p' for pawn...
     * @param square - location of the piece, initially start position on the board.
     * oppositeColor is useful for identifying enemy pieces.
     * hasMoved is used for pawn's initial 2-move rule and castling logic.
     */
    public Piece(char color, char type, Square square) {
        this.color = color;
        this.type = type;
        this.oppositeColor = getOppositeColor();
        this.square = square;
        this.hasMoved = false;
    }

    //various getters that is used during gameplay to gather info about a specific piece.
    public MovementPattern getMovement() {return this.movement;}
    public char getType() {
        return this.type;
    }
    public char getColor() {
        return this.color;
    }
    public char getOppositeColor() {
        return (this.color == 'w') ? 'b' : 'w';
    }
    public Square getSquare() {return this.square;}
    public void setSquare(Square square) {this.square = square;}


    /**
     * "Moves" are a list of squares that the piece can move to, according to its {@link MovementPattern} and the movement "scan" which was performed based on it.<br><br>
     *
     * Collecting this information is useful, for example when determining if a piece has an opposing king in its list of moves, which would logically correspond to a "check".
     */
    public List<Square> getMoves() {
        return this.legalMoves;
    }

    /**
     * "Moves" are a list of squares that the piece can move to, according to its {@link MovementPattern} and the movement "scan" which was performed based on it.<br><br>
     *
     * When a piece's {@link MovementPattern}} is used to gather a piece's moves, it should be stored as a field so that we can access it elsewhere.
     */
    public void setMoves(List<Square> moves) {
        this.legalMoves = moves;
    }

    /**
     * Adding moves without "setting" the entire move-list. This is only used for pawns, because they have more than one {@link MovementPattern} which together consist of their possible moves.
     */
    public void addMoves(List<Square> moves) {
        legalMoves.removeIf(moves::contains);
        this.legalMoves.addAll(moves);
    }

    /**
     * Is technically called every single time a piece is moved, but it only has effect the first time. It is used for the pawn's initial 2-move rule, and castling (which requires that the king and rook have not previously moved).
     */
    public void pieceHasMoved() {
        this.hasMoved = true;
    }

    /**
     *
     * @return
     */
    public boolean hasMoved() {return this.hasMoved;}




    //
    //psuedo-abstract methods which only exist to have an override in pawn. not sure if this was the most
    //logical design choice, but it's what made the most sense at the time :) :) :)

    /**
     * Override in {@link Pawn#changeMovePattern()}
     */
    public void changeMovePattern() {}

    /**
     * Override in {@link Pawn#isMovingPawn()}
     */
    public boolean isMovingPawn() {return false;}

}