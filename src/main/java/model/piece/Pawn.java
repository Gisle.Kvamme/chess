package model.piece;

import board.Square;

/**
 * The pawn justifies having its own class, due to its uniquely separated moving and attacking logic.
 */
public class Pawn extends Piece {

    private boolean isMoving;
    private boolean movePattern;



    public Pawn(char color, Square square) {
        super(color, 'p', square);
        this.hasMoved = false;
        this.isMoving = false;
        MovementPattern.setColor(this.color);
        movement = MovementPattern.UnmovedPawn;
    }


    /**
     * Returns whether the pawn is in its "move"-scan, in which we only want the squares "in front" of the pawn. If this is false, then we are instead interesting in its "attack squares", which are at each diagonal "in front" of the pawn.
     */
    @Override
    public boolean isMovingPawn() {return isMoving;}



    /**
     * A piece can only hold one movement pattern at a time, and this is only a problem for the pawn. <br><br>
     * Unlike all other pieces; pawns move and attack different squares. We therefore set their {@link MovementPattern} to one thing, perform a "scan" for moves, then switch the pattern and repeat the process, and this is done for each pawn each round.<br>
     * We use {@link #isMovingPawn()} in conjunction to the get desired results.<br><br>
     *
     */
    @Override
    public void changeMovePattern() {
        if (movePattern) {
            movement = (!this.hasMoved) ? MovementPattern.UnmovedPawn : MovementPattern.MovedPawn;
            movePattern = false;
            isMoving = true;
        } else {
            movement = MovementPattern.AttackingPawn;
            movePattern = true;
            isMoving = false;
        }
    }

}
