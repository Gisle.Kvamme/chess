package model;

import board.Board;
import board.Square;
import board.SquareItem;
import model.piece.*;

/**
 * A grid-based ChessBoard which extends the generic {@link Board} class.
 */
public class ChessBoard extends Board<Piece> {


    /**
     * Sets up a strictly 8x8 chess board. No experimentation allowed! My code is too fragile. >:(
     */
    public ChessBoard() {
        super(8, 8);
        setStandard();
    }

    /**
     * It's moronic to have this method here, as this job should be handled in the superclass, but it didn't want to work and I want to go to bed.
     */
    public void clearBoard() {
        for (int i = 0; i < this.getRanks(); i++) {
            for (int j = 0; j < this.getFiles(); j++) {
                this.set(new Square(i,j), null);
            }
        }
    }

    /**
     * Sets the current board to a standard chessboard to interact with. 64 squares, 16 white pieces, 16 black pieces
     */
    public void setStandard() {
        this.clearBoard();
        char[] backRank = new char[]{'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'};

        //BLACK SIDE
        for (int i = 0; i < getFiles(); i++) {
            this.set(new Square(1, i), new Pawn('b', new Square(1, i)));
        }
        for (int i = 0; i < backRank.length; i++) {
            this.set(new Square(0, i), new StandardPiece('b', backRank[i], new Square(0, i)));
        }

        //WHITE SIDE
        for (int i = 0; i < getFiles(); i++) {
            this.set(new Square(6, i), new Pawn('w', new Square(6, i)));
        }
        for (int i = 0; i < backRank.length; i++) {
            this.set(new Square(7, i), new StandardPiece('w', backRank[i], new Square(7, i)));
        }
    }


    /**
     * I don't think this method actually works lmao???
     */
    public ChessBoard getCopy(ChessBoard oldBoard) {
        ChessBoard newBoard = new ChessBoard();

        for (SquareItem<Piece> sq : oldBoard) {
            newBoard.set(sq.getSquare(), sq.getPiece());
        }
        return newBoard;
    }


    //FOR TESTING
    public static String asString(ChessBoard board) {
        String stringBuild = "";

        for (SquareItem<Piece> square : board) {
            if (square.getPiece() == null) {
                stringBuild += '*';
            } else {
                stringBuild += square.getPiece().getType();
            }

            if (square.getSquare().getFile() == 7) {
                stringBuild += '\n';
            }
        }
        return stringBuild;
    }

}

