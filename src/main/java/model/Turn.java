package model;

/**
 * Keeps track of the current state of the game. It's called Turn, but that's kind of a bad name, as it also checks whether you are in the menu or if the game is over... Oh well...
 */
public enum Turn {
    WHITE,
    BLACK,
    MENU,
    GAMEOVER,
    WAITING
}
