package model.ai;

import board.Square;
import model.piece.Piece;

/**
 * Record which keeps track of which piece can make which move.<br><br>
 * This is useful to the computer player as it saves large quantities of moves in various states.
 */
public record Move(Piece piece, Square square) {
    public Square getSquare(){return this.square;}
    public Piece getPiece(){return this.piece;}
}
