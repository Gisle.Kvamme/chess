package model.ai;


import board.Square;
import board.SquareItem;
import model.ChessBoard;
import model.ChessModel;
import model.piece.Piece;
import model.piece.StandardPiece;

import java.util.*;


/**
 * Hot-wired, somewhat-functional AI chess player. There's a lot of logic to it, and the code might
 * seem semi-reasonable, but it doesn't really work.<br>
 * A "real" chess engine would consider many more factors and look many more moves ahead, and spend a lot less system resources in doing so, but this is only here to give a functional example.<br>
 * <br><br>
 * It is mostly functional though, so there's that. :)
 */
public class Computer {

    private static final ChessModel model = new ChessModel(true);

    /**
     * @param type - the piece's char
     * @return integer representing an approximate value for the piece
     * <br><br>
     * This is a helper method for {@link Computer#evaluateMaterial(ChessBoard)} and allows<br>
     * {@link Computer#evaluateScore(ChessBoard)} to create a "score", which it will use to establish if a move is good for a player or not. If one player has more pieces than the other, they are obviously winning, and moreso if the pieces are considered to be more valuable.
     */
    private static int getPieceValue(char type) {
        return switch (type) {
            case 'p' -> 25;
            case 'N' -> 70;
            case 'B' -> 85;
            case 'R' -> 130;
            case 'Q' -> 225;
            case 'K' -> 1000;
            default -> 0;
        };
    }


    /**
     * Calls {@link Computer#getPieceValue(char)} on every piece on the board and evaluates a total score, where positive values favor white.
     * @param board - the board to assess
     * @return an approximation of the "score" of the board
     */
    public static int evaluateMaterial(ChessBoard board) {
        int sum = 0;
        for (SquareItem<Piece> sq : board) {
            if (board.get(sq.getSquare()) != null) {
                int operand = (sq.getPiece().getColor() == 'w') ? +1 : -1;
                sum += (getPieceValue(sq.getPiece().getType()) * operand);
            }
        }
        return sum;
    }


    /**
     * Takes in an integer n and returns whether that number is between two other integers.
     * <br><br>
     * @param n - a number which is to be tested.
     * @param start - the inclusive start of the range
     * @param stop - the inclusive end of the range
     * @return whether the number is within the range.
     */
    private static boolean inRange(int n, int start, int stop) {
        return (n >= start) && (n <= stop);
    }


    /**
     * @param sq - the square to be investigated
     * @return integer representing an approximate value for the square
     * <br><br>
     * This is a helper method for {@link Computer#evaluateSpace(ChessBoard)} and allows<br>
     * {@link Computer#evaluateScore(ChessBoard)} to create a "score", which it will use to establish if a move is good for a player or not. One of the ways that is determined is how many squares each player controls, as that implies that their pieces are more active.<br><br>
     * The center of the board is considered more valuable, as that is where piece activity is the highest. I have given these squares slightly more value, so that the computer favors them. This makes it at more likely to make a sound initial move, as "the fight for the center" is most important in the opening.
     */
    private int getSquareValue(Square sq) {
        if (((sq.getRank() == 7 || sq.getRank() == 0) && inRange(sq.getFile(), 0, 7))  ||  ((sq.getFile() == 7 || sq.getFile() == 0) && inRange(sq.getRank(), 0, 7)))
            return 1;
        else if (((sq.getRank() == 6 || sq.getRank() == 1) && inRange(sq.getFile(), 1, 6))  ||  ((sq.getFile() == 6 || sq.getFile() == 1) && inRange(sq.getRank(), 1, 6)))
            return 2;
        else if (((sq.getRank() == 5 || sq.getRank() == 2) && inRange(sq.getFile(), 2, 5))  ||  ((sq.getFile() == 5 || sq.getFile() == 2) && inRange(sq.getRank(), 2, 5)))
            return 3;
        else if (((sq.getRank() == 4 || sq.getRank() == 3) && inRange(sq.getFile(), 3, 4))  ||  ((sq.getFile() == 4 || sq.getFile() == 3) && inRange(sq.getRank(), 3, 4)))
            return 4;
        else
            return 0;
    }


    /**
     * Calls {@link Computer#getSquareValue(Square)} on every square of the board.<br>
     * @param board - the board to be assessed
     * @return the approximate "sum" of the value of the squares. White points are positive, black are negative.
     */
    private int evaluateSpace(ChessBoard board) {
        int sum = 0;
        for (SquareItem<Piece> boardSquare : board) {
            Piece p = board.get(boardSquare.getSquare());

            if (p != null) {
                int operand = (p.getColor() == 'w') ? +1 : -1;
                for (Square pieceSquare : p.getMoves()) {
                    sum += getSquareValue(pieceSquare) * operand;
                }
            }
        }
        return sum;
    }


    /**
     * Collects the king so that {@link Computer#evaluateKingSafety(ChessBoard)} can assess their safety.
     * @param board - the board in which the king resides
     * @param color - the color of the king
     * @return the king as a Piece
     */
    private Piece getKing(ChessBoard board, char color) {
        for (SquareItem<Piece> sq : board) {
            Piece p = sq.getPiece();

            if (p != null && p.getType() == 'K' && p.getColor() == color) {
                return p;
            }
        }
        throw new NullPointerException("The " + color + "king is missing from the board.");
    }


    /**
     * Assesses the safety of the kings on the board. A more exposed king results in a more vulnerable position, making it easier to attack the pieces around it and set it in check. This must therefore be taken into account.<br><br>
     * I accomplished this by checking how many squares the king is exposed to, by replacing it with a queen (which can see as far as possible in every direction) and collecting its squares.<br><br>
     * @param board - the board to asssess
     * @return an approximate "scoring" of the king's safety, where negative points favor white. (opposite of normal logic)
     */
    private int evaluateKingSafety(ChessBoard board) {
        int sum = 0;
        try {
            Piece whiteKing = getKing(board, 'w');
            Piece blackKing = getKing(board, 'b');

            Piece proxyQueenW = new StandardPiece('w', 'Q', whiteKing.getSquare());
            Piece proxyQueenB = new StandardPiece('b', 'Q', blackKing.getSquare());

            board.set(whiteKing.getSquare(), proxyQueenW);
            board.set(blackKing.getSquare(), proxyQueenB);
            model.setCurrentBoard(board);

            sum -= proxyQueenW.getMoves().size();
            sum += proxyQueenB.getMoves().size();

            board.set(whiteKing.getSquare(), whiteKing);
            board.set(blackKing.getSquare(), blackKing);
            model.setCurrentBoard(board);
            return sum;
        } catch (NullPointerException missingKings) {
            return 0;
        }
    }


    /**
     * Calls on all the evaluation methods to create a total score for the board, where positive values favor white. A total score of +23 means that white has a small advantage, a score of -100 is completely winning for black.<br><br>
     * Helped by {@link Computer#evaluateSpace(ChessBoard)} + {@link Computer#evaluateKingSafety(ChessBoard)} + {@link Computer#evaluateMaterial(ChessBoard)}
     */
    protected int evaluateScore(ChessBoard board) {
        return evaluateMaterial(board) + evaluateSpace(board) + evaluateKingSafety(board);
    }


    /**
     * Test method which takes in a board in a given state, performs a move, and returns what the score would be after that move has been performed.<br>
     * Makes it easy to test whether a move is advantagous or not.
     * <br><br>
     * @param board - the board to assess
     * @param move - the move to be performed in that board state
     * @return the score after the move has been made
     */
    public int evaluateAfterMove(ChessBoard board, Move move) {
        Piece pieceThatMoves = move.getPiece();
        Square originalPositionOfTheMovingPiece = move.getPiece().getSquare();
        Piece contentOfSquareToMoveTo = board.get(move.getSquare());

        board.set(originalPositionOfTheMovingPiece, null);
        board.set(move.getSquare(), pieceThatMoves);
        model.setCurrentBoard(board);

        int newScore = evaluateScore(board);

        board.set(originalPositionOfTheMovingPiece, pieceThatMoves);
        board.set(move.getSquare(), contentOfSquareToMoveTo);

        model.setCurrentBoard(board);

        return newScore;

    }


    /**
     * Collects all pieces on the board which are friendly to a given color.<br><br>
     * This is useful when getting the moves of a player, particularly in {@link Computer#getRandomMove}.
     */
    private List<Piece> getFriendlies(ChessBoard board, char color) {
        List<Piece> pieces = new ArrayList<>();

        for (SquareItem<Piece> sq : board) {
            if (sq.getPiece() != null && sq.getPiece().getColor() == color) {
                pieces.add(sq.getPiece());
            }
        }

        return pieces;
    }


    /**
     * Calls on various helpers to get a random move from a random pieces when given a color.<br>
     * Note: This method is encapsulated for public use in {@link Computer#getRandomMove(ChessBoard, char)}.
     * <br><br>
     * {@link Computer#getFriendlies(ChessBoard, char)} - to get the right-colored pieces.<br>
     * {@link Computer#getRandomPieceFromList(List)} - to get a random selection of one of these pieces.<br>
     * {@link Computer#getMoveListFromPiece(Piece)} - to get the moves of that piece.<br>
     * {@link Computer#getRandomMoveFromList(List)} - to get a random move from that piece's movelist.<br>
     */
    private Move getRandomMoveFromColor(ChessBoard board, char color) {
        List<Piece> allFriendlies = getFriendlies(board, color);
        List<Move> moveListOfRandomPiece = getMoveListFromPiece(getRandomPieceFromList(allFriendlies));

        while (moveListOfRandomPiece.size() < 1) {
            moveListOfRandomPiece = getMoveListFromPiece(getRandomPieceFromList(allFriendlies));
        }

        return getRandomMoveFromList(moveListOfRandomPiece);
    }


    /**
     * Goes through the movelist of a given piece and converts its squares to a move available to them.
     */
    private List<Move> getMoveListFromPiece(Piece p) {
        List<Move> moveList = new ArrayList<>();

        for (Square sq : p.getMoves()) {
            moveList.add(new Move(p, sq));
        }

        return moveList;
    }


    /**
     * Takes in a movelist and selects a random move from it.
     */
    private Move getRandomMoveFromList(List<Move> moveList) {
        return moveList.get(new Random().nextInt(moveList.size()));
    }


    /**
     * Takes in a piecelist and selects a random piece from it.
     */
    private Piece getRandomPieceFromList(List<Piece> pieceList) {
        return pieceList.get(new Random().nextInt(pieceList.size()));

    }


    /**
     * Generates a large selection of random moves, evaluates them. Then stores any unique moves as a pairing in a {@link HashMap}.<br>
     * @param board - the board in which the pieces reside
     * @param color - the color that is making the move
     * @param trials - the amount of moves that should be generated to create a sample. 100 is a decent number, as there is a limit to how many legal moves exist in a given position, but we also need "too many" in case of duplicates, which we don't keep.
     * @return a set of Moves and the evaluation of the position after that specific move has been made.
     */
    private HashMap<Move, Integer> getEvaluatedMoveSet(ChessBoard board, char color, int trials) {
        assert trials > 10;
        ChessBoard boardClone = new ChessBoard();
        boardClone = boardClone.getCopy(board);

        HashMap<Move, Integer> moveWithScores = new HashMap<>();
        for (int i = 0; i < trials; i++) {
            Move testMove = getRandomMoveFromColor(boardClone, color);
            int moveEvaluation = evaluateAfterMove(boardClone, testMove);
            if (!moveWithScores.containsKey(testMove))
                moveWithScores.put(testMove, moveEvaluation);

        }
        return moveWithScores;
    }


    /**
     * Filters through a HashMap<Move, Integer> and selects the one with the most favorable value to the given color.
     * <br><br>
     * @param set - a mapping of moves to its score-value, see {@link Computer#evaluateScore(ChessBoard)}.
     * @param color - the color that makes the moves
     * @param remove - if the move should be removed. this is not used at the moment, but could be used to do things like collecting the "top 5" from a set or whatever...
     * @return - the best Move that was found
     */
    private Move getBestFromEvaluatedMoveSet(HashMap<Move, Integer> set, char color, boolean remove) {
        int highScore = (color == 'w') ? -1000 : 1000;
        Move currentBestMove = null;

        for(Map.Entry<Move, Integer> entry: set.entrySet()) {
            if ((color == 'b' && entry.getValue() < highScore) || (color == 'w' && entry.getValue() > highScore)) {
                highScore = entry.getValue();
                currentBestMove = entry.getKey();
            }
        }

        if (remove)
            set.remove(currentBestMove);
        return currentBestMove;
    }


    /**
     * Gets a random move to play for the given color.
     */
    public Move getRandomMove(ChessBoard board, char color) {
        Move random = getRandomMoveFromColor(board, color);
        if (model.validateMove(random.getPiece(), random.getSquare()))
                return random;

        return getRandomMove(board, color);
    }


    /**
     * Gets a move that is decent, which means that it provides a good score and would be reasonable to play "in isolation", but does not take the response of the other player into consideration.<br><br>
     * This is done by validating the best move gained by {@link Computer#getBestFromEvaluatedMoveSet(HashMap, char, boolean)}.
     * <br><br>
     * This is further "smartened" by {@link Computer#getSmartMove(ChessBoard, char)}.
     * @param board - the
     * @param color
     * @return a decent move
     */
    public Move getDecentMove(ChessBoard board, char color) {
        HashMap<Move, Integer> set = getEvaluatedMoveSet(board, color, 100);
        Move currentBest = getBestFromEvaluatedMoveSet(set, color, true);

        if (currentBest != null && model.validateMove(currentBest.getPiece(), currentBest.getSquare())) {
            return currentBest;
        } else {
            return getRandomMove(board, color);
        }
    }


    /**
     * Gets the smartest possible move that I can muster with my limited abilities.<br><br>
     * Takes a one-move-ahead approach to AI and returns what is the best move it can find, given the barely-logical score evaluation system.
     * <br><br>
     * Code quality is sub-par, functionality is sub-par, runtime is sub-par. See ya in INF102.
     * <br><br>
     * @param board - current board
     * @param color - color that is moving
     * @return the smartest move
     */
    public Move getSmartMove(ChessBoard board, char color) {
        ChessBoard referenceBoard = new ChessBoard().getCopy(board);

        Move random = getRandomMove(board, color);
        int highScore = evaluateAfterMove(board, random);
        Move currentBest = random;

        for (int i = 0; i < 100; i++) {
            ChessBoard testBoard = new ChessBoard().getCopy(referenceBoard);
            Move decent = getRandomMove(board, color);

            Square squareOfPiece = decent.getPiece().getSquare();
            Piece originalContent = testBoard.get(decent.getSquare());

            model.setCurrentBoard(testBoard);
            testBoard.set(decent.getSquare(), decent.getPiece());
            testBoard.set(squareOfPiece, null);
            model.setCurrentBoard(testBoard);


            int worstPossibleScenario = evaluateScore(testBoard);

            for (int j = 0; j < 300; j++) {
                Move decentResponse = getRandomMove(testBoard, decent.getPiece().getOppositeColor());
                int evaluationAfterDecentResponse = evaluateAfterMove(testBoard, decentResponse);


                if ((color == 'b' && evaluationAfterDecentResponse > worstPossibleScenario) || (color == 'w' && evaluationAfterDecentResponse < worstPossibleScenario)) {
                    worstPossibleScenario = evaluationAfterDecentResponse;
                }
            }

            if ((color == 'b' && worstPossibleScenario < highScore) || (color == 'w' && worstPossibleScenario > highScore)) {
                highScore = worstPossibleScenario;
                currentBest = decent;
            }

            model.setCurrentBoard(testBoard);
            testBoard.set(decent.getSquare(), originalContent);
            testBoard.set(decent.getPiece().getSquare(), decent.getPiece());
            model.setCurrentBoard(testBoard);

        }
        return currentBest;
    }

    //the bad code ends here, you can open your eyes again.

}
