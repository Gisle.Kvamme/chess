package board;

import model.ChessModel;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * A board which holds onto a generic type, allowing us to modify it to any given game-type. In this case the generic type will be a {@link SquareItem} which will hold onto a piece.
 */
public class Board<E> implements IBoard<E> {

    private int rank;
    private int file;
    private List<List<E>> grid;


    /**
     * If no defaultValue is given, it is filled with nullvalues.
     * see @fillBoard method
     */
    public Board(int rank, int file){
        this.rank = rank;
        this.file = file;
        this.grid = fillBoard(rank, file, null);
    }

    public Board(int rank, int file, E defValue) {
        this.rank = rank;
        this.file = file;
        this.grid = fillBoard(rank, file, defValue);
    }
    

    /**
     * Fills a nested List of:
     * @param rank number of ranks
     * @param file number of fileumns
     * With the value of
     * @param defaultValue
     *
     * @return the nested List.
     */
    public List<List<E>> fillBoard(int rank, int file, E defaultValue){
        List<List<E>> board = new ArrayList<>();

        for (int r = 0; r < rank; r++) {
            board.add(r, new ArrayList<E>());
            for (int c = 0; c < file; c++) {
                board.get(r).add(defaultValue);
            }
        }
        return board;
    }


    /**
     * Getters to retrieve number of ranks and files, see super-doc in: {@link IBoard#getRanks()}
     */
    @Override
    public int getRanks() {
        return rank;
    }

    @Override
    public int getFiles() {
        return file;
    }


    /**
     * Alter a single square's contents, see super-doc in: {@link IBoard#set(Square, Object)}
     */
    @Override
    public void set(Square coordinate, E value) {
        grid.get(coordinate.getRank()).set(coordinate.getFile(), value);
    }


    /**
     * See super-doc in: {@link IBoard#get(Square)}
     */
    @Override
    public E get(Square coordinate) {
        if (squareIsOnGrid(coordinate))
            return grid.get(coordinate.getRank()).get(coordinate.getFile());
        return null;
    }


    /**
     * See super-doc in: {@link IBoard#squareIsOnGrid(Square)}
     */
    @Override
    public boolean squareIsOnGrid(Square coordinate) {
        int rank = coordinate.getRank();
        int file = coordinate.getFile();

        return (rank >= 0 && rank+1 <= this.rank) && (file >= 0 && file+1 <= this.file);
    }


    /**
     * Iteration object which will allow us to make our grid an iterable, which
     * will be used to get the contents of the squares in the board's current state.
     * <br><br>
     * Implementation example: {@link view.ChessView#drawBoard(Graphics2D, Iterable)}<br>
     * Here we use this iterator and iterate over a game-board via {@link ChessModel#boardIterator()} to draw the board with an appropriate appearance, and with "correct" content, i.e. the right pieces in the right places.
     */
    @Override
    public Iterator<SquareItem<E>> iterator() {
        ArrayList<SquareItem<E>> it = new ArrayList<>();

        for (int r = 0; r < rank; r++){
            for (int c = 0; c < file; c++) {
                it.add(new SquareItem<E>(new Square(r, c), grid.get(r).get(c)));
            }
        }

        return it.iterator();
    }
}
