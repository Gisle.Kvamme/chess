package board;


import model.ChessModel;
import view.ChessViewable;

/**
 * A generic game-board, which can be adapted and modified to fit the needs of a generic board-game.  In this particular example we are implementing a chess board which will be used to simulate a game of chess.
 */
public interface IBoard<E> extends Iterable<SquareItem<E>> {


    /**
     * Returns the number of ranks and files of the current gameboard. We use this to access the board's dimensions from anywhere in the program, which is, in one example, useful for drawing an appropriate-looking gameboard in the view. <br>
     * <br>
     * Implementation example:<br>
     * {@link model.ChessBoard#getRanks()} => {@link ChessModel#getRanks()} => {@link ChessViewable#getRanks()}.<br>
     * Here the board class that implements this method forwards the information to the model, which implements {@link ChessViewable} which again is then able to retrieve this information, allowing us to draw a nice chess-board pattern in the view.
     *
     * @return
     */
    int getRanks();
    int getFiles();


    /**
     * Sets the value of a postion in the grid. A subsequent call to {@link #get}
     * with an equal coordinate as argument will return the value which was set. The
     * method will overwrite any previous value that was stored at the location.
     * 
     * @param coordinate the location in which to store the value
     * @param value      the new value
     */
    void set(Square coordinate, E value);


    /**
     * Gets the content of a square on the board.
     *
     * @param coordinate The square which we wish to know the contents of.
     * @return a generic type, in this example representing a piece.
     */
    E get(Square coordinate);


    /**
     * Checks if a given square coordinate is "legal" in relation to the dimensions of the board.
     * <br><br>
     * @param coordinate - The square which we wish to "investigate"
     * @return - Returns a bool of whether or not it is legal.
     */
    boolean squareIsOnGrid(Square coordinate);
}

