package board;

import java.util.Objects;


/**
 * A square item holds a generic P, a type of piece, which is on a given {@link Square}, which represents that P's coordinates.
 */
public class SquareItem<P> {

    private Square Square;
    private P piece;


    public SquareItem(Square Square, P piece){
        this.Square = Square;
        this.piece = piece;
   }

    /**
     * Gets the SquareItem's Square, the coordinates of the given item.
     */
    public Square getSquare(){return this.Square;}


    /**
     * Gets the SquareItem's item, the item at a given coordinate.
     */
    public P getPiece(){return this.piece;}


    //STANDARD GENERATED METHOD
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SquareItem<?> that = (SquareItem<?>) o;
        return Objects.equals(Square, that.Square) && Objects.equals(piece, that.piece);
    }


    //STANDARD GENERATED METHOD
    @Override
    public int hashCode() {
        return Objects.hash(Square, piece);
    }


    //STANDARD GENERATED METHOD
    @Override
    public String toString() {
        return String.format("{ Square='{ row='%d', col='%d' }', item='%s' }", Square.getRank(), Square.getFile(), piece);
    }
}
