package board;

import java.util.Objects;


/**
 * A square, which will hold the coordinates of a given {@link SquareItem}, which will then hold a {@link model.piece.Piece} or a null-value, depending on the contents of the current game-board.
 */
public class Square {

    /**
     * Fields which represents the coordinates of the square.
     */
    private final int rank;
    private final int file;


    public Square(int rank, int file) {
        this.rank = rank;
        this.file = file;
    }


    /**
     * Gets the rank number of the current square.
     */
    public int getRank() {
        return rank;
    }


    /**
     * Gets the file number of the current square.
     */
    public int getFile() {
        return file;
    }




    //STANDARD GENERATED METHOD
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Square that = (Square) o;
        return rank == that.rank && file == that.file;
    }

    //STANDARD GENERATED METHOD
    @Override
    public int hashCode() {
        return Objects.hash(rank, file);
    }

    //STANDARD GENERATED METHOD
    @Override
    public String toString() {
        return String.format("{ rank='%d', file='%d' }", rank, file);
    }
}
