package tile;

/**
 * A record which holds the basic values of a tile which has been formed by the {@link TileCalculation}
 */
public record CalculatedTile(int currentX, int currentY, int newWidth, int newHeight) {


    /**
     * Gets the x and y of the current tile.
     */
    public int getX() { return this.currentX; }

    public int getY() { return this.currentY; }


    /**
     * Gets the width and height of the finished calculated tile.
     */
    public int getWidth() {
        return this.newWidth;
    }

    public int getHeight() {
        return this.newHeight;
    }
}
