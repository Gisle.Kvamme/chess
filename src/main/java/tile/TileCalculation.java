package tile;


/**
 * A class which takes in information about a frame (a drawn chess board, for instance) and the canvas it is to be painted on. These calculations are used to return new tiles using the record {@link CalculatedTile}. These tiles are scaled according to the size of the canvas and dimensions of the frame, and positioned according to the proportions and position of the previous tile.
 * <br><br>
 * A tile-calculation is set up for a given frame and canvas and then used to generate any new tile thereafter. For example, if we want to draw a chess board we set up a TileCalculation and then use it to generate 64 appropriately scaled and positioned instances of the record {@link CalculatedTile}.
 */
public class TileCalculation {

    private final int numberOfRows;
    private final int numberOfCols;
    private final int canvasHeight;
    private final int canvasWidth;
    private final int startX;
    private final int startY;


    /**
     *
     * @param numberOfRows - total number of rows in the given frame
     * @param numberOfCols - total number of cols in the given frame
     * @param canvasHeight - the coordinate height of the canvas in which the frame is painted on
     * @param canvasWidth - the coordinate width of the canvas in which the frame is painted on
     * @param startX - starting x-coordinate for the initial to-be-created {@link CalculatedTile}
     * @param startY - starting y-coordinate for the initial to-be-created {@link CalculatedTile}
     */
    public TileCalculation(int numberOfRows, int numberOfCols, int canvasHeight, int canvasWidth, int startX, int startY) {
        this.numberOfRows = numberOfRows;
        this.numberOfCols = numberOfCols;
        this.canvasHeight = canvasHeight;
        this.canvasWidth = canvasWidth;
        this.startX = startX;
        this.startY = startY;
    }


    /**
     * Returns a new tile with the given calculations, see notes above, which is further encapsulated using the record {@link CalculatedTile}
     */
    public CalculatedTile getCalculatedTile(int currentRank, int currentFile) {
        int Nrows = this.numberOfRows;
        int Ncols = this.numberOfCols;

        int currentX = startY + (currentFile * canvasWidth) / Ncols;
        int currentY = startX + (currentRank * canvasHeight) / Nrows;

        int nextX = startY + ((currentFile + 1) * canvasWidth) / Ncols;
        int nextY = startX + ((currentRank + 1) * canvasHeight) / Nrows;

        return new CalculatedTile(currentX, currentY, nextX-currentX, nextY-currentY);
    }
}
