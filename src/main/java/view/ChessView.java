package view;

import board.SquareItem;
import model.ChessModel;
import model.piece.Piece;
import tile.CalculatedTile;
import tile.TileCalculation;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;


public class ChessView extends JComponent {
    {
        // This code (between curly braces) is executed when an object is
        // created (before the call to the constructor, if one exists).

        // The call to setFocusable enables the panel to receive events from
        // the user, such as key-presses and mouse movements.
        this.setFocusable(true);
    }

    /**
     * A viewable object, see constructor.
     */
    private final ChessViewable viewable;

    /**
     *
     * @param viewable - A viewable object implements a Viewable interface. In this case {@link model.ChessModel}.
     */
    public ChessView(ChessViewable viewable) {
        this.viewable = viewable;
    }


    /**
     * @return - height of the current view. This is public to it accessible to {@link controller.ChessController}.
     */
    public int getViewHeight() {
        return this.getHeight();
    }

    /**
     * @return - width of the current view. This is public to it accessible to {@link controller.ChessController}.
     */
    public int getViewWidth() {
        return this.getWidth();
    }


    /**
     * Draws all the tiles on the board with correct color and piece. See helper methods below:
     * {@link #drawSingleTile(Graphics2D, int, int, int, int, Color)}, {@link #drawPiece(Graphics2D, Piece, CalculatedTile)}
     *
     * @param g        - a canvas to be painted

     * @param iterable - where tiles will be collected from - an iterable board
     * @throws IOException - see exception call in {@link #drawPiece}.
     */
    public void drawBoard(Graphics2D g, Iterable<SquareItem<Piece>> iterable) throws IOException {
        Color color = Color.LIGHT_GRAY;

        TileCalculation board = new TileCalculation(viewable.getRanks(), viewable.getFiles(), getViewHeight(), getViewWidth(), 0, 0);
        for (SquareItem<Piece> tile : iterable) {
            int rank = tile.getSquare().getRank();
            int file = tile.getSquare().getFile();
            CalculatedTile cTile = board.getCalculatedTile(rank, file);

            color = (color.equals(Color.LIGHT_GRAY)) ? Color.DARK_GRAY : Color.LIGHT_GRAY;
            if (file == 0) {
                color = (color.equals(Color.LIGHT_GRAY)) ? Color.DARK_GRAY : Color.LIGHT_GRAY;
            }
            this.drawSingleTile(g, cTile.getX(), cTile.getY(), cTile.getWidth(), cTile.getHeight(), color);

            if (tile.getPiece() != null)
                this.drawPiece(g, tile.getPiece(), cTile);
        }
    }


    /**
     * A display menu with buttons that will be drawn when {@link ChessModel#inMainMenu()} is true.
     */
    private void drawMainMenu(Graphics2D g) throws IOException {
        drawSingleTile(g, 0, 0, getWidth(), getHeight(), Color.BLACK);
        g.setColor(new Color(255, 255, 255));

        String directory = System.getProperty("user.dir") + "\\src\\main\\java\\view\\png\\mainmenu1.png";
        g.drawImage(ImageIO.read(new File(directory)), rToWidth(0.33), rToHeight(0.10), this.getWidth()/3, this.getHeight()/3, null);

        g.drawRect(rToWidth(0.25), rToHeight(0.45), this.getWidth()/2, this.getHeight()/8);
        g.drawRect(rToWidth(0.25), rToHeight(0.65), this.getWidth()/2, this.getHeight()/8);
        g.drawRect(rToWidth(0.25), rToHeight(0.85), this.getWidth()/2, this.getHeight()/8);
        this.drawText(g, "Battle a human", 20, true, Color.WHITE, 0.40, 0.50);
        this.drawText(g, "Battle the computer", 20, true, Color.WHITE, 0.38, 0.70);
        this.drawText(g, "Quit to desktop", 20, true, Color.WHITE, 0.40, 0.90);
    }


    /**
     * A display menu with buttons that will be drawn when {@link ChessModel#gameIsOver()} is true.
     */
    private void drawGameOverMenu(Graphics2D g) {
        this.drawCentralSquare(g);

        if (viewable.getCheckColor() == 'w')
            this.drawText(g, "Black won by checkmate.", 45, false, Color.WHITE, 0.15, 0.4);
        else if (viewable.getCheckColor() == 'b')
            this.drawText(g,  "White won by checkmate.", 45, false, Color.WHITE, 0.15, 0.4);
        else
            this.drawText(g,  "Draw by stalemate.", 45, false, Color.WHITE, 0.15, 0.4);

        g.setColor(new Color(255, 255, 255));
        g.drawRect(rToWidth(0.15), rToHeight(0.45), rToWidth(0.25), rToHeight(0.075));
        g.drawRect(rToWidth(0.60), rToHeight(0.45), rToWidth(0.25), rToHeight(0.075));

        this.drawText(g, "Return to menu", 12, true, Color.WHITE, 0.20, 0.48);
        this.drawText(g, "Exit to desktop", 12, true, Color.WHITE, 0.67, 0.48);
    }

    /**
     * Draws a neat little box that informs the player that they are waiting for the computer to move.
     */
    private void drawWaitingText(Graphics2D g) {
        g.setColor(new Color(255, 255, 255));
        g.drawRect(rToWidth(0.00), rToHeight(0.25), rToWidth(1.00), rToHeight(0.15));
        this.drawText(g, "Waiting for computer", 20, true, Color.BLACK, 0.10, 0.10);
    }


    /**
     * Helpers which return an int in relation to the height or width of the view.
     */
    private int rToWidth(double p) {
        return (int) (this.getWidth() * p);
    }
    private int rToHeight(double p) {
        return (int) (this.getHeight() * p);
    }





    /**
     * --- Helper for {@link ChessView#drawBoard} ---
     * Draws individual tile with a given color.
     * @param g - A canvas that the single piece will be painted on
     * @param x - The starting x-coordinate.
     * @param y - The starting y-coordinate.
     * @param width - Width of the tile, to create proper dimensions for {@link Graphics#fillRect}.
     * @param height - Height of the tile.
     * @param color - Color of the tile, called by {@link Graphics#fillRect}.
     * *
     */
    public void drawSingleTile(Graphics2D g, int x, int y, int width, int height, Color color) {
        g.setColor(color);
        g.fillRect(x, y, width, height);
    }


    /**
     * --- Helper for {@link ChessView#drawBoard} ---
     * @param g - canvas to paint on
     * @param p - the piece that is on the given tile, which is never null!
     * @param t - a tile with dimensions calculated with {@link tile.TileCalculation#getCalculatedTile(int, int)}

     * @throws IOException - in the case that the image file is missing or damaged.
     */
    public void drawPiece(Graphics2D g, Piece p, CalculatedTile t) throws IOException {
        Image img;
        if (p.equals(viewable.getSelectedPiece())) {
            img = ImageIO.read(new File(getImageDirectory(p, "selected")));
        } else if (p.getType() == 'K' && p.getColor() == viewable.getCheckColor()) {
            img = ImageIO.read(new File(getImageDirectory(p, "check")));
        } else
            img = ImageIO.read(new File(getImageDirectory(p, "default")));
        g.drawImage(img, t.getX()+(int) ((t.getWidth()*0.15)), t.getY()+ (int) (t.getHeight()*0.15), null);

    }

    /**
     * String-method which gets information about a piece, so that the correct image can be gathered.
     * The image files are named in convention of [piecetype][piececolor].png in various folders.<br>
     * --- Helper for {@link ChessView#drawPiece} => {@link ChessView#drawBoard} ---
     *
     * @param p - The piece, which serves as the basis, we will assess its color and type.
     * @param foldername - A foldername can be "selected" which gives a piece with a yellow hue.
     * @return - The filepath of the desired image according to the piece and the given specifics.
     */
    private static String getImageDirectory(Piece p, String foldername) {
        char color = p.getColor();
        char type = p.getType();

        return System.getProperty("user.dir") + "\\src\\main\\java\\view\\png\\" + foldername + "\\" + color + type + ".png";
    }

    /**
     * "Improved" version of Graphics' standard text-painting method.
     *
     * @param g - canvas to paint on
     * @param text - The text which is to be displayed
     * @param size - The text size
     * @param bold - bool => bold or not
     * @param color - text color
     * @param xInRelationToWidth - given as a double, i.e. 0.5 for text starting in the middle
     * @param yInRelationToHeight - given as a double, i.e. 0.5 for text starting in the middle
     */
    public void drawText(Graphics2D g, String text, int size, boolean bold, Color color, double xInRelationToWidth, double yInRelationToHeight) {
        if (bold)
            g.setFont(new Font("plaintext", Font.BOLD, size));
        else
            g.setFont(new Font("boldtext", Font.PLAIN, size));
        g.setColor(color);
        g.drawString(text, (int) (getWidth() * xInRelationToWidth), (int) (getHeight() * yInRelationToHeight));
    }

    /**
     * Makes a nice transparent block to draw text onto.
     */
    private void drawCentralSquare(Graphics2D g) {
        int startY = (int) (getHeight() * 0.30);
        int height = (int) (getHeight() * 0.25);
        Color color = new Color(0, 0, 0, 200);
        drawSingleTile(g, 0, startY, this.getWidth(), height, color);
    }


    /**
     * The paint method is called by the Java Swing framework every time either
     * // -- the window opens or resizes, or
     * // -- someone calls .repaint() on this object (note: do NOT call paint
     * // directly), or
     * // -- for some other reason Java Swing believes it is time for painting the canvas.
     * <p>
     * <p>
     */
    @Override
    public void paint(Graphics canvas) {
        super.paintComponent(canvas);
        Graphics2D graphics2D = (Graphics2D) canvas;

        //Set  anti-alias!
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        // Set anti-alias for text
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);


        //draw menu
        if (viewable.inMainMenu()) {
            try {
                this.drawMainMenu(graphics2D);
            } catch (IOException e) {
                e.printStackTrace();
            }
        //if we are not in the menu, the board should be visible, regardless of any other factor
        } else if (viewable.gameIsActive() || viewable.gameIsOver()) {
            try {
                this.drawBoard(graphics2D, viewable.boardIterator());
            } catch (IOException e) {
                e.printStackTrace();
            }
            //note: this doesn't display and I don't know how to fix it, but it's "there" in spirit :PP
            if (viewable.playerIsWaiting()) {
                this.drawWaitingText(graphics2D);
            }
            if (viewable.gameIsOver())
                this.drawGameOverMenu(graphics2D);

        }
        }
}
