package view;


import board.SquareItem;
import model.ChessModel;
import model.piece.Piece;

import java.awt.*;


/**
 * Interface for a viewable, in this case {@link ChessModel}. This particular model represents the "interactivity" of the chess board, which will be linked to a class which can visually represent the model using the Swing framework.
 */
public interface ChessViewable {


    /**
     * Allows the view access the model's board dimensions. This is to allow us to draw a chess-board
     * pattern with appropriate and scalable dimensions. Used in {@link ChessView#drawBoard(Graphics2D, Iterable)}.
     * <br><br>
     * Implementation: {@link ChessModel#getRanks()} and {@link ChessModel#getFiles()}.
     */
    int getRanks();
    int getFiles();


    /**
     * The iterable board, allowing us to draw an appropriate-looking board in the view.
     * <br><br>
     * Implementation: {@link ChessModel#boardIterator()}
     */
    Iterable<SquareItem<Piece>> boardIterator();


    /**
     * Lets the view know which piece is currently selected by the user's mouse input. This to allow
     * drawing it with yellow glow in the background.
     * <br>
     * @return the selected Piece, as an object in order for the view to know the piece's color and type.
     * <br><br>
     *
     * Implementation: {@link ChessModel#getSelectedPiece()}
     */
    Piece getSelectedPiece();


    /**
     * Lets the view know whether the game is over. If it is, we must draw some instructive graphics informing the players of it.
     *
     * @return a bool representing this fact.
     * <br><br>
     * Implementation: {@link ChessModel#gameIsOver()}
     */
    boolean gameIsOver();

    /**
     * Checks if our dear players are actually playing.
     * <br><br>
     * Implementation: {@link ChessModel#gameIsActive()}
     */
    boolean gameIsActive();

    /**
     * Checks if the player is waiting for the computer to make a move.
     * <br><br>
     * Implementation: {@link ChessModel#playerIsWaiting()}.
     */
    boolean playerIsWaiting();


    /**
     * Lets the view know if the state of the viewable model is in "menu"-mode or not.
     * <br><br>
     * Implementation: {@link ChessModel#inMainMenu()}.
     */
    boolean inMainMenu();

    /**
     * Lets the view know which king is in check, if any. This is to draw the appropriate king with a visually instructive red glow behind them.
     *
     * @return a char representing the king who is currently in check.<br>
     * If no king is in check, it will return a '?' - a nonsense character, which will not prompt any visual effect.
     * <br><br>
     * Implementation: {@link ChessModel#getCheckColor()}
     */
    char getCheckColor();



}
