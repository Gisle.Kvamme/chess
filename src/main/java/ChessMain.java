import controller.ChessController;
import model.ChessModel;
import model.ai.Computer;
import view.ChessView;
import java.util.InvalidPropertiesFormatException;

public class ChessMain {


    /**
     * TO-DO:
     */
    //   BONUS:
    //   drastically improve the AI
    //
    //   Computer tests need to be finished / ran again with better AI in place.
    //
    public static void main(String[] args) throws InvalidPropertiesFormatException {
        ChessModel model = new ChessModel(false);
        ChessView view = new ChessView(model);
        ChessController controller = new ChessController(model, view);
    }

}


