package controller;

import board.Square;
import board.SquareItem;
import model.piece.Piece;
import tile.CalculatedTile;
import tile.TileCalculation;
import view.ChessView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;


public class ChessController extends JFrame implements MouseListener {

    public static final JFrame frame = new JFrame("Semesteroppgave 2:  Åpen oppgave 'Sjakk mot computer'  -  INF101 V2022 - Gisle Illguth Kvamme");


    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * Edit if desirable. Mind you that a chessboard should always be a perfect square :)
     */
    public static final int frameHeight = 720;
    public static final int frameWidth = frameHeight;
    //////////////////////////////////////////////////////////////////////////////////////

    private ArrayList<JLabel> menuLabels = new ArrayList<>();
    private ArrayList<JLabel> squareLabels = new ArrayList<>();

    private final ChessControllable controllable;
    private final ChessView view;



    /**
     *
     * @param controllable - ChessModel implements ChessControllable
     * @param view - The view, which ChessModel interacts with via its {@link view.ChessViewable} implementations.
     * <br><br>
     *              We also create our basic frame, which we will add clickable JLabels to, which will
     *                 allow us to make clickable tiles which correspond to in-game squares. These squares
     *                are based on their visual counterparts, see {@link view.ChessViewable}. Then we can manipulate these
     *                "representations" with methods defined in in {@link ChessControllable}.
     */
    public ChessController(ChessControllable controllable, ChessView view){
        this.controllable = controllable;
        this.view = view;

        // The JFrame is the "root" application window.
        // We here set som properties of the main window,
        // and tell it to display our tetrisView
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setPreferredSize(new Dimension(frameWidth, frameHeight));
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);


        // Here we set which component to view in our window
        frame.setContentPane(view);

        // Call these methods to actually display the window
        frame.setVisible(true);

        setMainMenuLabels();
        //;
    }


    /**
     * Returns an int that is proportional to the width and height of the frame.<br>
     * Where 0.0 < p < 1.0<br>
     * A width of 0.50 would be half of the frame width.
     */
    private int rToWidth(double p) {
        return (int) (frameWidth * p);
    }
    private int rToHeight(double p) {
        return (int) (frameHeight * p);
    }


    /**
     * Creates three clickable labels with the help of {@link ChessController#createMenuLabel(int, int, int, int, String)}.<br>
     * The labels appear on the main menu and enables choosing between playing a human, a computer, or just quitting.
     */
    private void setMainMenuLabels() {
        frame.add(createMenuLabel((int) (frameWidth*0.25), (int) (frameHeight*0.50)-71, frameWidth/2, frameHeight/6, "human"), 0);
        frame.add(createMenuLabel((int) (frameWidth*0.25), (int) (frameHeight*0.70)-71, frameWidth/2, frameHeight/6, "comp"), 1);
        frame.add(createMenuLabel((int) (frameWidth*0.25), (int) (frameHeight*0.90)-71, frameWidth/2, frameHeight/6, "quit"), 2);
    }


    /**
     * Creates two clickable labels with the help of {@link ChessController#createMenuLabel(int, int, int, int, String)}.<br>
     * The labels appear when stalemate or checkmate is in the model. Lets the player return to the main menu or quit.
     */
    private void setGameOverLabels() {
        frame.add(createMenuLabel(rToWidth(0.15), rToHeight(0.45), rToWidth(0.25), rToHeight(0.075), "menu"));
        frame.add(createMenuLabel(rToWidth(0.60), rToHeight(0.45), rToWidth(0.25), rToHeight(0.075), "quit"));
    }


    /**
     * Creates standardized labels, which have a set position according to the params. We add it to a list so that we can easily clear labels later using {@link ChessController#clearLabels()}.
     */
    private JLabel createMenuLabel(int x, int y, int width, int height, String name) {
        JLabel j = new JLabel();
        j.setOpaque(true);
        j.addMouseListener(this);
        j.setBounds(x, y, width, height);
        j.setName(name);
        menuLabels.add(j);
        return j;
    }


    /**
     * Goes through all current assigned labels and removes them from the frame. I wish I could use the built-in JFrame method {@link JFrame#removeAll()}, but it didn't want to help.
     */
    private void clearLabels() {
        menuLabels.addAll(squareLabels);
        for (JLabel j : menuLabels) {
            frame.remove(j);
        }
        menuLabels = new ArrayList<>();  squareLabels = new ArrayList<>();
    }


    /**
     * Fills the board with clickable JLabels that are adjusted according to in-game squares.<br><br>
     *
     * Sizes are calculated according to {@link tile.TileCalculation}.
     */
    private void setSquaresAsLabels() {
        int nRanks = controllable.getRanks(), nFiles = controllable.getFiles();
        int viewHeight = view.getViewHeight(), viewWidth = view.getViewWidth();
        TileCalculation board = new TileCalculation(nRanks, nFiles, viewHeight, viewWidth, 0, 0 );

        for (SquareItem<Piece> square : controllable.boardIterator()) {
            int rank = square.getSquare().getRank();
            int file = square.getSquare().getFile();
            addToFrame(new JLabel(), board.getCalculatedTile(rank, file), rank, file);
        }
    }


    /**
     * --- Helper for {@link ChessController#setSquaresAsLabels()} ---<br><br>
     * Modifies a new JLabel to be clickable, and make it corresponds to a proper in-game tile. Each
     * tile will be given a name-tag in the form "[rank],[file]" which will be later converted to
     * numbers using {@link ChessController#nameTagToSquare(String[])}.<br><br>
     *
     * @param j - a new label which will be added to the frame
     * @param cTile - a {@link tile.CalculatedTile} ensures that the JLabel is identical to the visual representation
     * @param rank - the rank that corresponds to this JLabel
     * @param file - the file that corresponds to this JLabel
     */
    private void addToFrame(JLabel j, CalculatedTile cTile, int rank, int file) {
        j.setOpaque(true);
        j.addMouseListener(this);
        j.setBounds(cTile.getX(), cTile.getY(), cTile.getWidth(), cTile.getHeight());
        j.setName(String.format("%d,%d", rank, file));
        squareLabels.add(j);
        frame.add(j);
    }


    /**
     * Converts an array of two chars representing a square's rank and file into an appropriate object.<br>
     * <br>
     * @param rankAndFile - a 2-length array in the form [rank, file]
     * @return - a corresponding new Square(rank, file)
     */
    private Square nameTagToSquare(String[] rankAndFile) {
        int rank = Integer.parseInt(rankAndFile[0]), file = Integer.parseInt(rankAndFile[1]);
        return new Square(rank, file);
    }


    /**
     * Gets the nametag of a component (JLabels in this example) and selects the corresponding
     * in-game square via {@link ChessController#nameTagToSquare} and calling {@link model.ChessModel#select(Square)} on it.
     * <br><br>
     * This method is called by the overwritten MouseListener-methods. See docs below.
     */
    private void selectInModel(Component c) {
        if (!controllable.gameIsOver())
            controllable.select(nameTagToSquare(c.getName().split(",")));
        view.repaint();
    }


    /**
     * Uses the nametag of the label created in {@link ChessController#createMenuLabel(int, int, int, int, String)}<br>
     * to identify which user action is performed.
     */
    private void selectInMainMenu(Component c) {
        clearLabels();
        if (c.getName().equals("quit")) {
            System.exit(0);
        } else if (c.getName().equals("human")) {
            setSquaresAsLabels();
            controllable.startGame();
        } else if (c.getName().equals("comp")) {
            setSquaresAsLabels();
            controllable.startCPUGame();
        }
        view.repaint();
    }


    /**
     * Uses the nametag of the label created in {@link ChessController#setGameOverLabels()}
     * to identify which user action is performed.
     */
    private void selectInGameOver(Component c) {
        clearLabels();
        setGameOverLabels();
        if (c.getName().equals("quit")) {
            System.exit(0);
        } else if (c.getName().equals("menu")) {
            clearLabels();
            controllable.setToMainMenu();
            setMainMenuLabels();
        }
        view.repaint();
    }


    /**
     * Selects a component that corresponds to which game mode we are currently in.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (controllable.inMainMenu()) {
            selectInMainMenu(e.getComponent());
        } else if (controllable.gameIsOver()) {
            selectInGameOver(e.getComponent());
        } else {
            selectInModel(e.getComponent());
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (!(controllable.inMainMenu()) || (controllable.gameIsOver()))
            selectInModel(e.getComponent());
    }


    /**
     * Converts the current mousepoint into its matching Component, so that when we release
     * the mouse it will behave as if we manually selected the square that the MouseEvent occured on.
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        Point p = ((Component) e.getSource()).getLocation();
        e.translatePoint((int) p.getX(), (int) p.getY());
        Component tile = frame.getContentPane().getComponentAt(e.getX(), e.getY());

        if (!controllable.gameIsOver() && !controllable.inMainMenu())
            selectInModel(tile);
    }


    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
