package controller;

import board.Square;
import board.SquareItem;
import model.ChessModel;
import model.piece.Piece;


/**
 * A controllable, in this case {@link ChessModel}. This particular model represents the "interactivity" of the chess board, which will be linked to a controller class which can manipulate the interactive elements using a user-controlled MouseListener.
 */
public interface ChessControllable {


    /**
     * Lets the controller know the dimensions of the board, allowing us to create the appropriate amount of JLabels.
     * <br><br>
     * Implementation: {@link ChessModel#getRanks()} and {@link ChessModel#getFiles()}.
     */
    int getRanks();
    int getFiles();


    /**
     * Lets the controller select squares on the board using the MouseListener and various helpers.
     * <br><br>
     * Implementation: {@link ChessModel#select(Square)}.
     */
    void select(Square square);


    /**
     * Lets the controller know if the game has ended, so that it may stop listening for MouseEvents.
     * <br><br>
     * Implementation: {@link ChessModel#gameIsOver()}.
     */
    boolean gameIsOver();


    /**
     * If you click the "start game" button, it starts a normal human-human game.
     * <br><br>
     * Implementation: {@link ChessModel#startGame()}.
     */
    void startGame();


    /**
     * If you click the "start vs computer", it should start a game that facilitates human-computer gameplay in the controllable.
     * <br><br>
     * Implementation: {@link ChessModel#startCPUGame()}.
     */
    void startCPUGame();


    /**
     * Checks whether the model is set to be in the main menu or not.
     * <br><br>
     * Implementation: {@link ChessModel#inMainMenu()}.
     */
    boolean inMainMenu();


    /**
     * If you return to the main menu after a finished game, it should change the state of the model to be in the menu as well.
     * <br><br>
     * Implementation: {@link ChessModel#setToMainMenu()}.
     */
    void setToMainMenu();


    /**
     * Allows us to iterate over the squares of the board so that we may create corresponding JLabels.
     * <br><br>
     * Implementation: {@link ChessModel#boardIterator()}.
     */
    Iterable<SquareItem<Piece>> boardIterator();

}
