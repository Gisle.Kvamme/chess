<hr>

# Chess: The Game of Kings
<hr>

<img height="300" src="C:\Users\gisle\Downloads\INF101\chess\md\chesslogo.png" class="center" width="300"/><br>


<p style="text-align: center;"><b>Welcome to Chess!<br><br></b><br><b>A Mandatory Assignment in</b><br><b>INF101 - "Object-oriented programming"<br><br></b>Gisle Illguth Kvamme (gkv012)<br>University of Bergen<br>Department of Informatics</p>
<br>
<hr>


<br><br>
Chess is a two-player "war game" that was invented in 7th century Persia, and has been in constant development ever since.<br><br>
The object of the game is simple: checkmate your opponent. This is accomplished when your opponent's king is under attack, and have no way of getting out.<br><br> At this point, the game is over, and the attacking player is declared the winner.
<br><br>
<hr>



<br>
<br>
<hr>

## Program instructions

- Run the program and you will reach the main menu, from here you have three choices:
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\mainmennu.png" width="200"/>
</figure>

<hr>

#### Battle a human
A local match that will run in the same window. Battle your friend, stepfather, or possibly even group leader.
<br><br>
#### Battle the computer
Take on the dumbest supercomputer in the world in an epic battle to the death... Or at least until it crashes when it can't find a move.
<br><br>
#### Quit to desktop
For when you've realised that you might as well be playing on an actual chess board.
<br><br>
#### Secondary menu
When the game is over, you are able to return to the main menu or quit, giving the possbility for continuous gameplay.
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\gameOver.png" width="200"/>
</figure>

<br><br><br><br>
- The two first mentioned will begin a standard-rules (mostly) game of chess. If you're confident in your abilities in the "Game of Kings" then this might be the end of this README. Otherwise, press on!
<br>
<br>
<hr>

## How to play: Basic rules
The general gameplay is fairly simple:<br>
- White makes the first move:
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\whiteStarts.png" width="200"/>

</figure>
- Black responds:
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\blackResponds.png" width="200"/>
</figure>
- This pattern continues until somebody wins.<br>
<br>
- Players are free to make any possible move they want, unless they are in check:
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\checkForced.png" width="200"/>
  <figcaption>White would love to capture black's knight, but has to find a way to get out of check first.</figcaption>
</figure>
- What happens if they can't get out of check?
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\checkMate.png" width="200"/>
  <figcaption>The black queen attacks the white's from the front, while the rook prevents escape backwards. The white king has no escape!</figcaption>
</figure>
- The game is over! Black wins.<br><br><br>
- But what if you can't make a move, but you're also not in check?
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\staleMate.png" width="200"/>
  <figcaption>The white king has no escape! But is not in check.</figcaption>
</figure>
- This is a draw by stalemate.

<br><br>




<hr>



## How to play: Movement
<hr>


<figure>
  <figcaption><b>Pawn</b></figcaption>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\pawnmoves.png" width="200"/>
  <figcaption>Pawns move once forward or attack once diagonally.</figcaption>
</figure>
<br>
<figure>
  <figcaption><b>Bishop</b></figcaption>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\bishopmoves.png" width="200"/>
  <figcaption>The bishop can move and attack continuously on all diagonals.</figcaption>
</figure>
<br>
<figure>
  <figcaption><b>Rook</b></figcaption>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\rookmoves.png" width="200"/>
  <figcaption>The rook can move and attack continuously on all ranks and files.</figcaption>
</figure>
<br>
<figure>
  <figcaption><b>Knight</b></figcaption>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\knightmoves.png" width="200"/>
  <figcaption>The knight moves and attacks in an L-shape.</figcaption>
</figure>
<br>
<figure>
  <figcaption><b>Queen</b></figcaption>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\queenmoves.png" width="200"/>
  <figcaption>The queen can move and attack continuously in all directions.</figcaption>
</figure>
<br>
<figure>
  <figcaption><b>King</b></figcaption>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\kingmoves.png" width="200"/>
  <figcaption>The king can move and attack once in all directions.</figcaption>
</figure>



<br>
<br>
<hr>

## How to play: Special movement

<hr>
<br>


### "Two-move rule"
The pawn is allowed to move two moves forward if it has not previously moved (this includes attacking).
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\twomove.png" width="200"/>
  <figcaption>The arrows indicate the possible moves for each pawn.</figcaption>
</figure>
<br>

### Castling
Castling is a great way to ensure king safety. It can be performed by either side if the king and the rook on the side that you're castling on have not previously moved.<br><br>
It can be performed on the king-side by moving your king to g1 or g8.

<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\kingcastle1.png" width="200"/>
</figure>

<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\kingcastle2.png" width="200"/>
  <figcaption>White creates a king-side castle, switching his king's position with his rook.</figcaption>
</figure>
<br>
Castling can be performed on the queen-side by moving your king to c1 or c8.
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\queencastle1.png" width="200"/>
</figure>
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\queencastle2.png" width="200"/>
  <figcaption>White creates a queen-side castle, switching his king's position with his rook.</figcaption>
</figure>
<br><br>
Remember that castling is a king move, and you are still not allowed to put yourself in check.
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\castleillegal.png" width="200"/>
  <figcaption>White may still not castle in this position.</figcaption>
</figure>
<br>

### Promotion
Most of the strategies around the "endgame" of a game of chess revolves around the battle to promote pawns.<br><br>
This occurs when a pawn reaches the "end" of the board. That means the 8th rank for white, and the 1st rank for black.
At this stage, they are promoted to a queen.

<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\promotion1.png" width="200"/>
  
</figure>
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\promotion2.png" width="200"/>
  <figcaption>White moves his pawn from b7 to b8, which causes it to become a queen.</figcaption>
</figure>


<br>
<br>
<hr>


## Deviations from "real chess"
- "En passant" capture is not possible, which is annoying to program and rarely occurs in real gameplay.
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\enpassant.png" width="200"/>
  <figcaption>Diagram showing a pawn capture "in passing".</figcaption>
</figure>
<br>
- In normal chess, the may promote to any piece that's not a pawn or a king. However, there is never any real reason to do so, therefore it has been omitted from this program.
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\promotion3.png" width="200"/>
  <figcaption>The promotion screen on lichess.org.</figcaption>
</figure>
<br>
- Draws are not possible outside of stalemates, and if you reach a position where neither player can win, the game will be stuck.
<figure>
  <img height="200" src="C:\Users\gisle\Downloads\INF101\chess\md\drawposition.png" width="200"/>
  <figcaption>One of the conditions where a win is impossible.</figcaption>
</figure>

<br>
<br><br>
<br><br>
<br>
<hr><hr>


Written by Gisle Illguth Kvamme (gkv012)<br><br><br>
Images taken from the board editor of:
http://www.lichess.org/
<hr><hr>